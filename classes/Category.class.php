<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 07.04.2019
 * Time: 6:19
 */

class Category
{
    /**
     * Category id
     * @var int
     */
    public $id;

    /**
     * Parent category id
     * @var int
     */
    public $parent_id;

    /**
     * Category title
     * @var string
     */
    public $title;

    /**
     * Unique category string name
     * @var string
     */
    public $url;

    /**
     * recipe collection
     * @var array
     */
    public $recipes;

    /**
     * @var null|PDO
     */
    private $pdo = null;

    public $page_limit = 20;

    public $current_page = 0;
    /**
     * subcategory url collection
     * @var array
     */
    public $subcategories;

    public $count_pages;
    public $page_offset = 0;
    public $pager_name = 'collection';

    public function __construct()
    {
        $this->initPdo();
    }

    public function init($hash, $offset = 1)
    {

        $this->initOffset($offset);

        $this->initCategory($hash);

        $this->setCountPages();
        $this->initSubCategories();
        $this->initRecipesByCategoryId();
    }

    private function initOffset($offset){
        $this->current_page = $offset;
        if($offset) {
            $this->page_offset = --$offset;
            $this->page_offset *= $this->page_limit;
        }
    }

    public function initSuperCategory($offset=1) {
        $this->initOffset($offset);
        $this->initSubCategories();
        $ids = [];
        foreach ($this->subcategories as $row){
            $ids[] = $row['id'];
        }
        $ids = implode(',', $ids);
        $this->initCountSuperPages($ids);
        $this->initRecipesByCategoryIds($ids);
    }

    public function initCountSuperPages($ids){
        $sql = 'SELECT COUNT(*) AS recipes_num FROM oc_recipe WHERE category_id IN('.$ids.')';
        $stm = $this->pdo->query($sql);
        if(!$stm) return false;
        $num = $stm->fetch(PDO::FETCH_ASSOC);
        $this->count_pages = ceil($num['recipes_num']/$this->page_limit);
        return $this->count_pages;
    }

    public function initRecipesByCategoryIds($ids) {
        $sql = 'SELECT 
                    r.id,
                    r.category_id,
                    r.title,
                    r.img,
                    r.shot_description,
                    r.full_description,
                    r.pieces_num,
                    r.hash_id,
                    r.page_name,
                    r.original_id,
                    r.cook_time,
                    r.loc_path_img,
                    c.url AS category_url
                FROM oc_recipe r 
                INNER JOIN sub_category AS c ON r.category_id=c.id 
                WHERE r.category_id IN('.$ids.') LIMIT ' . $this->page_limit . ' OFFSET '.$this->page_offset;

        $stm = $this->pdo->query($sql);
        if(!$stm) return false;

        $this->recipes = $stm->fetchAll(PDO::FETCH_ASSOC);
        return $stm->rowCount();
    }

    public function initRecipesByCategoryId() {
        $sql = 'SELECT 
                    r.id,
                    r.category_id,
                    r.title,
                    r.img,
                    r.shot_description,
                    r.full_description,
                    r.pieces_num,
                    r.hash_id,
                    r.page_name,
                    r.original_id,
                    r.cook_time,
                    r.loc_path_img,
                    c.url AS category_url
                FROM oc_recipe r 
                INNER JOIN sub_category AS c ON r.category_id=c.id 
                WHERE r.category_id=:category_id LIMIT ' . $this->page_limit . ' OFFSET '.$this->page_offset;

        $stm = $this->pdo->prepare($sql);
        if(!$stm) return false;
        $stm->bindParam(':category_id', $this->id);

        $stm->execute();
        $this->recipes = $stm->fetchAll(PDO::FETCH_ASSOC);
        return $stm->rowCount();
    }

    /**
     * Get category data
     * @param $category_id
     * @return bool|mixed
     */
    public function selectCategoryById($category_id){
        $sql = 'SELECT id,
                    parent_id,
                    title,
                    url
                    FROM sub_category WHERE id=:category_id';
        $stm = $this->pdo->prepare($sql);
        if(!$stm) return false;
        $stm->bindParam(':category_id', $category_id);
        $stm->execute();
        return $stm->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Get category data by hash id
     * @param $category_id
     * @return bool|mixed
     */
    public function selectCategoryByHash($hash_id){
        $sql = 'SELECT id,
                    parent_id,
                    title,
                    url
                    FROM sub_category WHERE hash_id=:hash_id';
        $stm = $this->pdo->prepare($sql);
        if(!$stm) return false;
        $stm->bindParam(':hash_id', $hash_id);
        $stm->execute();
        return $stm->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Init category data
     * @param $hash
     * @return bool
     */
    public function initCategory($hash) {
        $data = $this->selectCategoryByHash($hash);
        if(empty($data)) return false;
        $this->id = $data['id'];
        $this->parent_id = $data['parent_id'];
        $this->title = $data['title'];
        $this->url = $data['url'];
        return true;
    }

    public function recipeExists(){
        return !(is_null($this->recipes) or empty($this->recipes));
    }

    private function initSubCategories(){
        $sql = 'SELECT url,title,id
                    FROM sub_category WHERE parent_id=:parent_id';
        $stm = $this->pdo->prepare($sql);
        if(!$stm) return false;
        $stm->bindParam(':parent_id', $this->parent_id);
        $stm->execute();
        $this->subcategories = $stm->fetchAll(PDO::FETCH_ASSOC);
        return $stm->rowCount();
    }


    /**
     * Init database PDO resource
     */
    private function initPdo() {
        $this->pdo = DB::getInstance();
    }

    private function setCountPages(){
        $sql = 'SELECT COUNT(*) AS recipes_num FROM oc_recipe WHERE category_id=:category_id';
        $stm = $this->pdo->prepare($sql);
        if(!$stm) return false;
        $stm->bindParam(':category_id', $this->id);
        $stm->execute();
        $num = $stm->fetch(PDO::FETCH_ASSOC);
        $this->count_pages = ceil($num['recipes_num']/$this->page_limit);
        return $this->count_pages;
    }
}