<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.04.2019
 * Time: 22:10
 */

class DB
{
    /**
     * @var null
     */
    private static $_pdo = null;

    private static $DB_HOST = 'localhost';
    private static $DB_NAME = 'food';
    private static $DB_USER = 'root';
    private static $DB_PASS = '#MCgNewPhs3s';

    private function __construct () {}

    /**
     * @return null|PDO
     */
    public static function getInstance()
    {
       if(is_null(self::$_pdo)) {
           self::$_pdo = new PDO(
               'mysql:host=' . self::$DB_HOST . ';dbname=' . self::$DB_NAME,
               self::$DB_USER,
               self::$DB_PASS,
               [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]
           );
       }

        return self::$_pdo;
    }

    private function __clone () {}
    private function __wakeup () {}

}