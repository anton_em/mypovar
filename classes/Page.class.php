<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 07.04.2019
 * Time: 7:17
 */

class Page
{
    /**
     * Page handler name
     * @var string
     */
    private $handler;

    private $hash;

    private $handler_param;

    public function getDynamicPage() {
        if($this->getHandler()) {
            // подключение обработчиков handler_id определен
            include_once($this->handler.'.php');
            // вызываем обработчик динамической страницы с параметрами
            echo call_user_func($this->handler, $this->handler_param);
        }
    }

    private function getHandler(){

        // главная
        if(count($_SERVER['REQUEST_URI']) == 1 and strstr('/', $_SERVER['REQUEST_URI'])){
            $this->handler = 'home';
            return true;
        }

        $nav = explode('/', $_SERVER['REQUEST_URI']);

        if(!is_array($nav) or empty($nav)) return false;

        unset($nav[0]);

        $rest_val = md5(end($nav));

        prev($nav);
        $route = current($nav);

        switch($route){
            case 'menu':
                $this->handler_param = ['hash' => $rest_val];
                $this->handler = 'supercategory';
                if(count($nav) !== 2) $this->handler = 'page404';
                break;
            case 'collection':
                $this->handler_param = ['hash' => $rest_val];
                $this->handler = 'category';
                if(count($nav) !== 2) $this->handler = 'page404';
                break;
            case 'recipe':
                $this->handler_param = ['hash' => $rest_val];
                $this->handler = 'recipe';
                if(count($nav) !== 2) $this->handler = 'page404';
                break;
            case 'page':

                $page_num = $this->getRestPageNum();

                if($nav[1] == 'collection') {
                    $this->handler_param = [
                            'hash' => $this->getPagerCategory(),
                            'page_offset' => $page_num
                        ];
                    $this->handler = 'pager';
                } elseif($nav[1] == 'menu') {
                    $this->handler_param = [
                        'hash' => md5($this->getRestValByParamName('menu')),
                        'page_offset' => $page_num
                    ];
                    $this->handler = 'supercategory';
                } else {
                    $this->handler = 'page404';
                }

                if(count($nav) !== 4) $this->handler = 'page404';

                break;
            default:
                $this->handler = 'page404';
        }
        return true;
    }

    private function getPagerCategory(){
        return md5($this->getRestValByParamName('collection'));
    }

    private function getRestPageNum(){
        return intval($this->getRestValByParamName('page'));
    }

    private function getRestValByParamName($param_name){
        $nav = explode('/', $_SERVER['REQUEST_URI']);
        for($i=0; $i<count($nav); $i++){
            if($nav[$i] == $param_name){
                return $nav[++$i];
            }
        }
        return false;
    }

}