<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 04.04.2019
 * Time: 22:06
 */

class Recipe
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $category_id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $img;

    /**
     * @var string
     */
    public $shot_description;

    /**
     * @var string
     */
    public $full_description;

    /**
     * @var int
     */
    public $pieces_num;

    /**
     * @var string
     */
    public $hash_id;

    /**
     * @var string
     */
    public $page_name;

    /**
     * @var int
     */
    public $original_id;

    /**
     * @var string
     */
    public $category_name;

    /**
     * @var string
     */
    public $category_url;

    /**
     * @var string
     */
    public $main_img_url;

    /**
     * @var string
     */
    public $similar_recipes_view;

    /**
     * @var array|bool
     */
    public $ingredient;

    /**
     * @var array|bool
     */
    public $step;

    /**
     * @var array
     */
    public $similar_recipes;

    /**
     * @var array
     */
    public $prev_recipe;

    /**
     * @var array
     */
    public $next_recipe;

    /**
     * @var null|PDO
     */
    private $pdo = null;


    public function __construct()
    {
        $this->initPdo();
    }

    private function initPdo() {
        $this->pdo = DB::getInstance();
    }

    /**
     * @param $hash
     * @return bool|int
     */
    public function init($hash) {

        $data = $this->selectRecipe($hash);

        if(empty($data)) return false;

        $this->id = $data['id'];
        $this->category_id = $data['category_id'];
        $this->title = $data['title'];
        $this->img = $data['loc_path_img'];
        $this->shot_description = $data['shot_description'];
        $this->full_description = $data['full_description'];
        $this->pieces_num = $data['pieces_num'];
        $this->category_url = $data['category_url'];
        $this->page_name = $data['page_name'];

        $this->ingredient = $this->selectIngredient($this->id);
        $this->step = $this->selectSteps($this->id);
        $this->main_img_url = $this->getMainImgUrl();

        $this->prev_recipe = $this->getPrevRecipe();
        if(!$this->prev_recipe) {
            $this->prev_recipe = $this->getLastRecipe();
        }

        $this->next_recipe = $this->getNextRecipe();

        if(!$this->next_recipe) {
            $this->next_recipe = $this->getFirstRecipe();
        }

        $this->similar_recipes = $this->getSimilarRecipe();
        if (count($this->similar_recipes) != 20 ) {
            $this->similar_recipes = array_merge(
                $this->similar_recipes,
                $this->getAdditionSimilarRecipe(20 - count($this->similar_recipes))
            );
        }

        foreach ($this->prev_recipe as $row)  {
            $this->prev_url = $this->getRecipeUrl($row['page_name']);
            $this->prev_title = $row['title'];
        }
        foreach ($this->next_recipe as $row)  {
            $this->next_url = $this->getRecipeUrl($row['page_name']);
            $this->next_title = $row['title'];
        }

        return $this->id;
    }

    /**
     * @param $hash
     * @return bool|mixed
     */
    public function selectRecipe($hash) {
        $sql = 'SELECT
  r.id,
  r.category_id,
  r.title,
  r.img,
  r.shot_description,
  r.full_description,
  r.pieces_num,
  r.hash_id,
  r.page_name,
  r.original_id,
  r.loc_path_img,
  c.url AS category_url
FROM oc_recipe r
  INNER JOIN sub_category AS c ON r.category_id=c.id
WHERE r.hash_id=:hash';

        $stm = $this->pdo->prepare($sql);
        if(!$stm) return false;
        $stm->bindParam(':hash', $hash);
        $stm->execute();
        return $stm->fetch(PDO::FETCH_ASSOC);
    }

    public function getRecipeUrl($uri) {
        return '/recipe/'.$uri;
    }

    public function getNextRecipe(){
        $sql = 'SELECT * FROM oc_recipe WHERE id > :id AND category_id=:category_id LIMIT 1';
        $stm = $this->pdo->prepare($sql);
        $stm->bindParam(':id', $this->id, PDO::PARAM_INT);
        $stm->bindParam(':category_id', $this->category_id, PDO::PARAM_INT);
        if(!$stm) return false;
        $stm->execute();
        if(!$stm->rowCount()) return false;
        return  $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getSimilarRecipe() {

        $res = array();

        $sql = 'SELECT * FROM oc_recipe WHERE id > :id AND category_id=:category_id LIMIT 21';

        $stm = $this->pdo->prepare($sql);

        $stm->bindParam(':category_id', $this->category_id);
        $stm->bindParam(':id', $this->id);
        if(!$stm) return false;
        $stm->execute();

        if($stm->rowCount()==0) return $res;
        $res = $stm->fetchAll(PDO::FETCH_ASSOC);
        array_shift($res);
        return $res;
    }

    public function getAdditionSimilarRecipe($count){
        $shift = false;
        if($count==20) {
            $count++;
            $shift=true;
        }
        $res = array();
        $sql = 'SELECT * FROM oc_recipe WHERE category_id=:category_id LIMIT '.$count;
        $stm = $this->pdo->prepare($sql);
        $stm->bindParam(':category_id', $this->category_id);
        if(!$stm) return $res;
        $stm->execute();
        if($shift){
            $res = $stm->fetchAll(PDO::FETCH_ASSOC);
            array_shift($res);
            return $res;
        }
        $res = $stm->fetchAll(PDO::FETCH_ASSOC);

        return $res;
    }

    public function getPrevRecipe() {
        $sql = 'SELECT * FROM oc_recipe WHERE id = (SELECT MAX(id) from oc_recipe where  category_id=:category_id AND id<:id)';
        $stm = $this->pdo->prepare($sql);
        $stm->bindParam(':id', $this->id, PDO::PARAM_INT);
        $stm->bindParam(':category_id', $this->category_id, PDO::PARAM_INT);
        if(!$stm) return false;
        $stm->execute();
        if(!$stm->rowCount()) return false;
        return  $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getLastRecipe() {
        $sql = 'SELECT * FROM oc_recipe WHERE category_id=:category_id AND id=(SELECT MAX(id) FROM oc_recipe WHERE category_id=:category_id) ';
        $stm = $this->pdo->prepare($sql);
        $stm->bindParam(':category_id', $this->category_id, PDO::PARAM_INT);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);

    }

    public function getFirstRecipe() {
        $sql = 'SELECT * FROM oc_recipe WHERE category_id=:category_id AND id=(SELECT MIN(id) FROM oc_recipe WHERE category_id=:category_id) ';
        $stm = $this->pdo->prepare($sql);
        $stm->bindParam(':category_id', $this->category_id, PDO::PARAM_INT);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);

    }

    public function getMainImgUrl() {
        return $this->img;
    }

    public function getRecipeImgUrl($img) {
        return '/'.Image::IMG_DIR.'/'. $this->category_url . '/'.$img. '.jpg';
    }

    public function getImgUrl($name) {
        return Image::IMG_DIR.'/'. $this->category_url . '/' . $name;
    }

    /**
     * @param $recipe_id
     * @return array|bool
     */
    private function selectIngredient($recipe_id) {
        $sql = 'SELECT description FROM oc_ingredient WHERE recipe_id=:recipe_id';
        $stm = $this->pdo->prepare($sql);
        if(!$stm) return false;
        $stm->bindParam(':recipe_id', $recipe_id, PDO::PARAM_INT);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $recipe_id
     * @return array|bool
     */
    private function selectSteps($recipe_id){
        $sql = 'SELECT num_steps, img, description FROM oc_step WHERE recipe_id=:recipe_id';
        $stm = $this->pdo->prepare($sql);
        if(!$stm) return false;
        $stm->bindParam(':recipe_id', $recipe_id, PDO::PARAM_INT);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

}