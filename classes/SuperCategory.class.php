<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12.06.2019
 * Time: 9:22
 */

class SuperCategory
{
    public $id;
    public $parent_id;
    public $title;
    public $url;
    public $pager_name = 'category';
    public $content;
    /**
     * subcategory url collection
     * @var array
     */
    public $subcategories;

    private $pdo;

    public function __construct()
    {
        $this->initPdo();
    }

    public function init($hash){
        $this->initCategory($hash);
        $this->initSubCategories();
    }

    /**
     * Init category data
     * @param $hash
     * @return bool
     */
    public function initCategory($hash) {
        $data = $this->selectCategoryByHash($hash);
        if(empty($data)) return false;
        $this->id = $data['id'];
        $this->parent_id = $data['parent_id'];
        $this->title = $data['title'];
        $this->url = $data['url'];
        return true;
    }

    private function initSubCategories(){
        $sql = 'SELECT url,title
                    FROM sub_category WHERE parent_id=:parent_id';
        $stm = $this->pdo->prepare($sql);
        if(!$stm) return false;
        $stm->bindParam(':parent_id', $this->id);
        $stm->execute();
        $this->subcategories = $stm->fetchAll(PDO::FETCH_ASSOC);
        return $stm->rowCount();
    }

    /**
     * Get category data by hash id
     * @param $category_id
     * @return bool|mixed
     */
    public function selectCategoryByHash($hash_id){
        $sql = 'SELECT id,
                    parent_id,
                    title,
                    url
                    FROM oc_super_category WHERE hash_id=:hash_id';
        $stm = $this->pdo->prepare($sql);
        if(!$stm) return false;
        $stm->bindParam(':hash_id', $hash_id);
        $stm->execute();
        return $stm->fetch(PDO::FETCH_ASSOC);
    }


    private function initPdo(){
        $this->pdo  = DB::getInstance();
    }

}