<?php
/**
 * Created by PhpStorm.
 * User: Emelyanov Anton
 * Date: 16.12.2018
 * Time: 9:02
 */

class Template
{
    const TEMPLATE_TYPE = '.tpl';

    public function getFullPage($str){
        $res =  array();
        $data = get_object_vars($this->data);
        foreach($data as $key=>$val) {
            if(is_array($val)) continue;
            $res[$key] = $val;
        }
        return $this->getHtmlFromArray($res, $str);
    }

    public function getHtmlByTemplateName($data, $template_name) {
        $template = $this->getTemplate($template_name);
        return $this->getHtmlFromArray($data, $template);
    }

    public function getHtmlFromArray($data, $str) {
        if (!is_array($data)) return 'First param expected array in getHtmlFromArray function';
        $keys = array_keys($data);
        $search = array();

        foreach ($keys as $key) {
            if (is_array($data[$key])) continue;
            $search[] = $this->getPattern($key);
        }
        return str_replace($search, $data, $str);
    }
    public function getHtmlItemByStr($data, $item, $str){
        $search = $this->getPattern($item);
        return str_replace($search, $data, $str);
    }

    public function getHtmlItem($data, $item, $template_name) {
        $template = $this->getTemplate($template_name);
        $search = $this->getPattern($item);
        return str_replace($search, $data, $template);
    }


    public function getTemplate($filename) {
        $url =  $filename . self::TEMPLATE_TYPE;
        return file_get_contents($url, true);
    }

    public function getSubMenu($menu_data) {
        if (count($menu_data) == 0) return '';
        $delimiter = $this->getTemplate('sub_menu/delimiter');

        $html = '';
        $i = 1;
        foreach ($menu_data as $key => $val) {

            if ($key == 'current_item') {
                $html .=
                    $this->getHtmlByTemplateName(
                        array('current_item' => $val),
                        'sub_menu/current_submenu_item'
                    );
            } else {
                $html .= $this->getHtmlByTemplateName(
                    array('link' => $val['link'], 'title' => $val['title']),
                    'sub_menu/submenu_item'
                );
            }

            if ($i != count($menu_data)) {
                $html .= $delimiter;
            }
            $i++;
        }
        return $this->getHtmlByTemplateName(
            array('items' => $html),  'sub_menu/sub_menu'
        );
    }

    public function getPager($link_name, $first_page, $current_page, $count_pages) {

        $pagination = '';
        $template_dir = 'pager/';

        /* параметры пагинации */
        // номер страницы
        $page = 1;
        // смещение от центра блока
        $offset = 5;
        // центр блока
        $center = $offset + 1;
        // кол-во кнопок
        $count_button = $offset * 2 + 1;

        /* инициализация пагинации */
        $page_data = array();
        $page_data['link_name'] = $link_name;
        $page_data['first_link'] = $first_page;
        $page_data['last_link'] = $link_name . $count_pages;
        $page_data['prev_link'] = $link_name . ($current_page - 1);
        $page_data['next_link'] = $link_name . ($current_page + 1);

        $next_nav = '';
        if ($count_button < $count_pages) {

            // вывод кнопок первая и предыдущая страница
            if ($current_page > $center) {
                $pagination .= $this->getHtmlByTemplateName($page_data, $template_dir . 'prev');
                // смещение курсора влево от центра блока пагинации
                $page = $current_page - $offset;
            }

            // перемещение курсора по последним кнопкам
            if ($current_page > ($count_pages - $offset)) {
                $page = $count_pages - $count_button + 1;
            }

            // следущая и последняя страница
            if ($current_page < $count_pages - $offset) {
                $next_nav = $this->getHtmlByTemplateName($page_data, $template_dir . 'next');
            }
        }

        for ($i = 0; $i < $count_button; $i++) {

            if ($page > $count_pages ) break;

            // номер страницы
            $page_data['num_page'] = $page;
            // ссылка
            $page_data['link'] = $page_data['link_name'] . $page_data['num_page'];

            // устанавливаем первую ссылку
            if ($page == 1) {
                $page_data['link'] = $page_data['first_link'];
            }

            // вывод кнопки
            if ($page == $current_page) {
                $pagination .=
                    $this->getHtmlByTemplateName($page_data, $template_dir . 'pagination_current_item');
            } else {
                $pagination .=
                    $this->getHtmlByTemplateName(
                        $page_data,
                        $template_dir. 'pagination_item'
                    );
            }
            $page++;
        }

        // вывод всего блока пагинации
        return $this->getHtmlByTemplateName(
            array('items' => $pagination . $next_nav),
            $template_dir . 'pagination'
        );
    }


    private function getPattern ($name) {
        return "[$name]";
    }
}