<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 21.04.2019
 * Time: 22:03
 */

class CategoryView extends PageView
{

    /**
     * Init data for templating
     * @param Category $category
     */
    public function dataInit(Category $category) {
        $this->data = $category;
        $this->title = $category->title;
    }

    /**
     * @return bool|string
     */
    function getMain()
    {
        return $this->getContent();
    }

    public function getContent($flag=false) {
        $html = '';
        foreach ($this->data->recipes as $row) {
            $row['recipe_url'] = '/recipe/' . $row['page_name'];
            $row['cook_time'] = str_replace(array('PT','M'), '', $row['cook_time']);
            $row['main_img_url'] = $row['loc_path_img'];
            $html .= $this->getHtmlByTemplateName($row, 'category/recipe');
        }
        $data = [
            'recipes'=>$html,

            'pager' => $this->getPager('/'.$this->data->pager_name.'/'.$this->data->url.'/page/',
                '/'.$this->data->pager_name.'/'.$this->data->url,
                $this->data->current_page,
                $this->data->count_pages
            ),
            'title'=>$this->data->title];
        $data['subcategories_list'] = '';
        if($flag){
            $data['subcategories_list'] = $this->getSubCategoriesItemList($this->data->subcategories);
        }
        return $this->getHtmlByTemplateName($data, 'category/category');
    }

    private function getSubCategoriesItemList($subcategories_url_collection){

        $html = '';

        foreach ($subcategories_url_collection as $row) {
            $url = '/collection/'.$row['url'];
            $html .= $this->getHtmlByTemplateName([
                'subcategories_url'=>$url,
                'subcategories_title'=>$row['title']],  'category/subcategory');
        }
        return $html;
    }
}