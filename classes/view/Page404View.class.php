<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.07.2019
 * Time: 7:05
 */

class Page404View extends PageView
{
    public function getMain(){
        return $this->get404();
    }
}