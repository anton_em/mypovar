<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 21.04.2019
 * Time: 21:17
 */

interface PageInterface
{
    public function getHeader();

    public function getFooter();

    public function getMain();

    public function getPage();
}