<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 07.04.2019
 * Time: 8:49
 */

abstract class PageView extends Template implements PageInterface
{
    protected $data;

    public $title = 'Рецепты';

    abstract function getMain();

    public function getPage() {
        $html = $this->getHeader();
        $html .= $this->getMain();
        $html .= $this->getFooter();
        return $html;
    }

    public function getHeader() {
        return $this->getHtmlByTemplateName(['title'=> $this->title], 'header');
    }


    public function getFooter() {
        return $this->getTemplate('footer');
    }

    // получение шаблона и заполнение
    // данными совпадающими в темплате и объекте или дополнение из входящего масcива
    public function getFillTemplate($out, $data) {

        if($out) {
            $replace = array();
            $matches = array();
            $i = 0;

            if(preg_match_all('/\[([^\]]+?)\]/m', $out,$matches)) {

                foreach ($matches[1] as $val) {

                    if(isset($data->$val)) {
                        $replace[$i] = $data->$val;
                    }
                    $i++;
                }
                $out = str_replace($matches[0],$replace,$out);
            }
            unset($matches,$replace,$i);
        }

        return $out;
    }

    public function getPage404(){
        $html = $this->getHeader();
        $html .= $this->get404();
        $html .= $this->getFooter();
        return $html;
    }

    public function get404(){
        header("HTTP/1.0 404 Not Found");
        return $this->getTemplate('page404');
    }


}