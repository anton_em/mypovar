<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 07.04.2019
 * Time: 7:47
 */

class RecipeView extends PageView
{
    /**
     * Get main content on page
     * @return string
     */
    public function getMain()
    {
        return $this->getRecipe();
    }

    /**
     * Init data for templating
     * @param Recipe $recipe
     */
    public function dataInit(Recipe $recipe) {
        $this->data = $recipe;
    }

    /**
     * @return string
     */
    public function getRecipe() {

        $ingredients = $this->getIngredients();
        $steps = $this->getSteps();
        $this->getSimilarRecipes();
        $html = $this->getHtmlItem($ingredients, 'ingredients', 'recipe/recipe');
        $html = $this->getHtmlItemByStr($steps, 'steps', $html);
        $html = $this->getFullPage($html);

        return $html;
    }

    public function getSimilarRecipes(){
        $html = '';
        foreach($this->data->similar_recipes as $row) {
            $row['similar_url'] = $this->data->getRecipeUrl($row['page_name']);
            $row['similar_img'] = $row['loc_path_img'];

            $html .= $this->getHtmlByTemplateName($row, 'recipe/similar_item');
        }
        $this->data->similar_recipes_view = $html;
        return $html;
    }

    /**
     * Get instruction list
     * @return string
     */
    private function getSteps() {
        $html = '';
        foreach ($this->data->step as $row) {
            $html .= $this->getHtmlFromArray(
                array(
                    'num_steps' => $row['num_steps'],
                    'description' => $row['description']
                ),
                $this->getTemplate('recipe/step')
            );
        }
        return $html;
    }

    /**
     * Get ingredient list
     * @return string
     */
    private function getIngredients(){
        $html = '';
        foreach ($this->data->ingredient as $row) {
            $html .= $this->getHtmlItem(
                $row['description'], 'ingredient',  'recipe/ingredient'
            );
        }
        return $html;
    }



}