<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12.06.2019
 * Time: 9:46
 */

class SuperCategoryView extends PageView
{
    /**
     * Init data for templating
     * @param Category $category
     */
    public function dataInit(SuperCategory $category) {
        $this->data = $category;
        $this->title = $category->title;
    }

    /**
     * @return bool|string
     */
    function getMain()
    {
        return $this->getContent();
    }

    public function getContent() {

        return $this->getHtmlByTemplateName([

            'content'=> $this->data->content,

            'title'=>$this->data->title], 'super_category/super_category');

    }

    private function getSubCategoriesItemList($subcategories_url_collection){
        $html = '';

        foreach ($subcategories_url_collection as $row) {
            $url = '/collection/'.$row['url'];
            $html .= $this->getHtmlByTemplateName([
                'subcategories_url'=>$url,
                'subcategories_title'=>$row['title']],  'category/subcategory');
        }
        return $html;
    }

}