<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 21.04.2019
 * Time: 22:16
 */


/**
 * @param $param
 * @return string
 */
function category($param){

    $category = new Category();
    $view = new CategoryView;


    $category->init($param['hash']);

    $view->dataInit($category);

    if(is_null($category->id)) return $view->getPage404();


    return $view->getPage();

}