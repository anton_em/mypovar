<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18.05.2019
 * Time: 6:26
 */
function pager($param){
    $category = new Category();
    $view = new CategoryView();

    $category->init($param['hash'], $param['page_offset']);

    if(!$category->recipeExists() or
        $param['page_offset'] == 0 or
        $param['page_offset'] == 1) return $view->getPage404();

    $view->dataInit($category);

    return $view->getPage();

}