<?php
/**
 * @param $param
 * @return string
 */
function recipe($param){

    $recipe = new Recipe();
    //$category = new Category();
    //$image = new Image();
    $view = new RecipeView();

    $recipe->init($param['hash']);

    if(is_null($recipe->id)) return $view->getPage404();

    $view->dataInit($recipe);

    return $view->getPage();

}