<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12.06.2019
 * Time: 8:07
 */

function supercategory($param){

    $super_category = new SuperCategory();
    $view = new SuperCategoryView();

    $sub_category = new Category();
    $view_sub_category = new CategoryView();

    $super_category->init($param['hash']);

    if(!$super_category->id) return $view->getPage404();

    $sub_category->parent_id = $super_category->id;

    $offset = 0;

    if(isset($param['page_offset'])) $offset = $param['page_offset'];

    $sub_category->initSuperCategory($offset);
    $sub_category->pager_name = 'menu';
    $sub_category->url = $super_category->url;

    if(!$sub_category->recipeExists()) return $view->getPage404();

    $view_sub_category->dataInit($sub_category);

    $super_category->content = $view_sub_category->getContent(true);

    $view->dataInit($super_category);

    return $view->getPage();
}