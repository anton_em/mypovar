<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 07.04.2019
 * Time: 7:22
 */
define('CLASSES_DIR','classes');
define('CLASSES_VIEW_DIR','view');
define('TEMPLATES_DIR','template');
define('HANDLERS_DIR','handlers');
define('LOG_DIR','log');

set_include_path(get_include_path().PATH_SEPARATOR.ROOT_DIR);
set_include_path(get_include_path().PATH_SEPARATOR.ROOT_DIR.DIRECTORY_SEPARATOR.CLASSES_DIR);
set_include_path(get_include_path().PATH_SEPARATOR.ROOT_DIR.DIRECTORY_SEPARATOR.TEMPLATES_DIR);
set_include_path(get_include_path().PATH_SEPARATOR.ROOT_DIR.DIRECTORY_SEPARATOR.HANDLERS_DIR);
set_include_path(get_include_path().PATH_SEPARATOR.ROOT_DIR.DIRECTORY_SEPARATOR.LOG_DIR);
set_include_path(get_include_path().PATH_SEPARATOR.ROOT_DIR.DIRECTORY_SEPARATOR.CLASSES_DIR.DIRECTORY_SEPARATOR.CLASSES_VIEW_DIR);

spl_autoload_register(function ($class) {include ($class .'.class.php');});