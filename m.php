<?php
exit;
ini_set("memory_limit","512M");
ini_set('max_execution_time', 36000);
error_reporting(E_ALL);
ini_set('display_errors',1);

require_once 'init.php';

/*while(ob_get_level()) ob_end_clean();
header('Connection: close');
ignore_user_abort();
ob_start();
echo('Connection Closed' . date(' Y-m-d H:i:s'));
$size = ob_get_length();
header("Content-Length: $size");
ob_end_flush();
flush();*/

$db = DB::getInstance();
$stm = $db->query('select id, img from oc_recipe');
$data = $stm->fetchAll(PDO::FETCH_ASSOC);


file_put_contents('img.log', 'Start: ' . date('Y-m-d H:i:s') .PHP_EOL, FILE_APPEND);

foreach ($data as $row) {
    //$last_id = file_get_contents('id');
    if($row['id'] <= 65049) continue;
    $loc_path = loadImg($row['img']);
    $sql = "update oc_recipe set loc_path_img='$loc_path' where id={$row['id']}";
    $db->query($sql);
    file_put_contents('img.log', date('Y-m-d H:i:s ')."{$row['id']}: $loc_path" .PHP_EOL, FILE_APPEND);
    //file_put_contents('id', $row['id']);

}
file_put_contents('img.log', 'Finish: ' . date('Y-m-d H:i:s') .PHP_EOL, FILE_APPEND);


function loadImg($img) {
if(!$img) return false;

$img_arr = explode('/', $img);
$name = end($img_arr);

$arr_dir = [substr(md5(microtime()), mt_rand(0, 30), 2) , substr(md5(microtime()), mt_rand(0, 30), 2) ,
    substr(md5(microtime()), mt_rand(0, 30), 2),substr(md5(microtime()), mt_rand(0, 30), 2)];

$buff_dir = '/img';
$i = 0;
foreach ($arr_dir as $dir) {
    $buff_dir .= '/' . $dir;

    if(!is_dir(__DIR__.$buff_dir))
        mkdir(__DIR__.$buff_dir, 0777, true);

}

$src = file_get_contents($img);
$path = $buff_dir .'/'.$name;
file_put_contents(__DIR__.$path, $src);
return $path;
}