<div id="middle">
    <div class="headline cmsmasters_color_scheme_default">
        <div class="headline_outer">
            <div class="headline_color"></div><div class="headline_inner align_center  cmsmasters_breadcrumbs">
                <div class="headline_inner_content">
                    <ul style="text-align: left;">
                        [subcategories_list]
                    </ul>
                    <div class="headline_aligner"></div><div class="headline_text"><h1 class="entry-title">[title]</h1></div><div class="cmsmasters_breadcrumbs"><div class="cmsmasters_breadcrumbs_aligner"></div><div class="cmsmasters_breadcrumbs_inner"><a href="/" class="cms_home">Главная</a>
                            <span class="breadcrumbs_sep"> / </span>
                            <span>[title]</span></div></div></div></div></div>
    </div><div class="middle_inner">
        <div class="content_wrap fullwidth">
            <style>
                .pagination ul {
                    text-align: center;
                }
                .pagination li {
                    background: #fff;
                    border-style: solid;
                    border-width: 1px;
                    border-color: #d3d3d3;
                    line-height: 30px;
                    margin-right: -1px;
                    display: inline-block;
                    padding: 0 8px;
                }
                .pagination li:before {
                    content: none;
                }
                .subcat-item {
                    display: inline-block;
                    margin-right: 10px
                }
                .subcat-item:before {
                    margin: 0;
                    font-size: 20px;
                    bottom: 2px;
                }
            </style>

            <!-- Start Content -->
            <div class="middle_content entry"></div></div><div id="cmsmasters_row_50210d9614" class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
            <div class="cmsmasters_row_outer_parent">
                <div class="cmsmasters_row_outer">
                    <div class="cmsmasters_row_inner">
                        <div class="cmsmasters_row_margin cmsmasters_11">
                            <div id="cmsmasters_column_9c8b1b3e87" class="cmsmasters_column one_first">
                                <div class="cmsmasters_column_inner"><div id="portfolio_8d1764d8ca" class="cmsmasters_wrap_portfolio entry-summary" data-layout="grid" data-layout-mode="perfect" data-url="" data-orderby="name" data-order="ASC" data-count="8" data-categories="baking,breakfast,dessert,dinner,snacks,%d1%81ocktails" data-metadata="title,categories,rollover"><div class="portfolio grid large_gap perfect cmsmasters_4 isotope" style="position: relative; height: 2024.56px;"><!-- Start Project Grid Article -->
                                            [recipes]
                                        </div>
                                        [pager]
                                </div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="cl"></div><div class="content_wrap fullwidth">

            <div class="middle_content entry"></div>
            <!-- Finish Content -->



        </div>
    </div>
</div>