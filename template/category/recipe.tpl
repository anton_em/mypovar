<!-- Start Project Grid Article -->
<article id="post-13182" class="cmsmasters_project_grid post-13182 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-dinner shortcode_animated" data-category="dinner" style="width: 485px; position: absolute; left: 485px; top: 0px;">
    <div class="project_outer">
        <div class="project_outer_image_wrap">
            <div class="project_outer_image_wrap_cont">
                <div class="cmsmasters_project_info_item">
                    <div class="cmsmasters_project_info_item_inner">время приготовления: [cook_time] мин.</div>
                </div>
                <div class="cmsmasters_project_info_item">
                    <div class="cmsmasters_project_info_item_inner">порции: [pieces_num]</div>
                </div>
            </div>
            <figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="[main_img_url]" class="full-width wp-post-image" alt="[title]" title="[title]">
                <div class="cmsmasters_img_rollover"><a href="[recipe_url]" title="[title]" class="cmsmasters_open_link"></a></div>
            </figure>
        </div>
        <div class="project_inner">
            <header class="cmsmasters_project_header entry-header">
                <h4 class="cmsmasters_project_title entry-title"><a href="[recipe_url]">[title]</a></h4>
            </header>
            <footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">[cook_time] мин.</span><span class="cmsmasters_project_category">
                    <!--<a href="#dinner" class="cmsmasters_cat_color cmsmasters_cat_41" rel="category tag">Dinner</a>-->
                </span></footer>
        </div>
    </div>
</article>
<!-- Finish Project Grid Article -->