<a href="javascript:void(0)" id="slide_top" class="cmsmasters_theme_icon_slide_top"><span></span></a>
</div> <!-- Finish Main -->

<!-- Start Footer -->
<footer id="footer">
    <div class="footer cmsmasters_color_scheme_footer cmsmasters_footer_default">
        <div class="footer_inner">
            <div class="footer_logo_wrap"><a href="/" title="Good Food recipe" class="footer_logo">
                    <img src="/wp-content/themes/good-food/theme-vars/theme-style/img/logo_footer.png" alt="Good Food" />
                    <img class="footer_logo_retina" src="/wp-content/themes/good-food/theme-vars/theme-style/img/logo_footer_retina.png" alt="Good Food" width="285" height="55" />
                </a>
            </div><div class="footer_nav_wrap"><nav><div class="menu-footer-navigation-container"><ul id="footer_nav" class="footer_nav">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home"><a href="#">Home</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#">Recipes</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#">About Us</a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom "><a href="#">Events</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page "><a href="#">Blog</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#">Contacts</a></li>
                        </ul></div></nav></div>
            <div class="social_wrap">
                <div class="social_wrap_inner">
                    <ul>
                        <li>
                            <a href="#" class="cmsmasters_social_icon cmsmasters_social_icon_1 cmsmasters-icon-linkedin cmsmasters_social_icon_color" title="Linkedin"></a>
                        </li>
                        <li>
                            <a href="#" class="cmsmasters_social_icon cmsmasters_social_icon_2 cmsmasters-icon-facebook-1 cmsmasters_social_icon_color" title="Facebook" target="_blank"></a>
                        </li>
                        <li>
                            <a href="#" class="cmsmasters_social_icon cmsmasters_social_icon_3 cmsmasters-icon-google cmsmasters_social_icon_color" title="Google" target="_blank"></a>
                        </li>
                        <li>
                            <a href="#" class="cmsmasters_social_icon cmsmasters_social_icon_4 cmsmasters-icon-twitter cmsmasters_social_icon_color" title="Twitter" target="_blank"></a>
                        </li>
                        <li>
                            <a href="#" class="cmsmasters_social_icon cmsmasters_social_icon_5 cmsmasters-icon-vimeo cmsmasters_social_icon_color" title="Vimeo" target="_blank"></a>
                        </li>
                    </ul>
                </div>
            </div>		<span class="footer_copyright copyright">
			<a class="privacy-policy-link" href="/privacy-policy">Privacy Policy</a> GoodFoodRecipe.ru © 2020 / All Rights Reserved		</span>
        </div>
    </div></footer>
<!-- Finish Footer -->

</div>
<span class="cmsmasters_responsive_width"></span>
<!-- Finish Page -->

<div id="cookie-law-info-bar"><span>Мы используем файлы cookie, чтобы предоставлять вам наилучшие услуги на нашем веб-сайте. Нажимая «Принимаю», вы соглашаетесь с нашими стандартными настройками. <a href="#" data-cli_action="accept" id="cookie_action_close_header"  class="medium cli-plugin-button cli-plugin-main-button cookie_action_close_header cli_action_button" >Принять</a></span></div><div id="cookie-law-info-again"><span id="cookie_hdr_showagain">Privacy & Cookies Policy</span></div><div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
<div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>
<script type="text/javascript">
    /* <![CDATA[ */
    cli_cookiebar_settings='{"animate_speed_hide":"500","animate_speed_show":"500","background":"#FFF","border":"#b1a6a6c2","border_on":false,"button_1_button_colour":"#000","button_1_button_hover":"#000000","button_1_link_colour":"#fff","button_1_as_button":true,"button_1_new_win":false,"button_2_button_colour":"#333","button_2_button_hover":"#292929","button_2_link_colour":"#444","button_2_as_button":false,"button_3_button_colour":"#000","button_3_button_hover":"#000000","button_3_link_colour":"#fff","button_3_as_button":true,"button_3_new_win":false,"button_4_button_colour":"#000","button_4_button_hover":"#000000","button_4_link_colour":"#fff","button_4_as_button":true,"font_family":"inherit","header_fix":false,"notify_animate_hide":true,"notify_animate_show":false,"notify_div_id":"#cookie-law-info-bar","notify_position_horizontal":"right","notify_position_vertical":"bottom","scroll_close":false,"scroll_close_reload":false,"accept_close_reload":false,"reject_close_reload":false,"showagain_tab":true,"showagain_background":"#fff","showagain_border":"#000","showagain_div_id":"#cookie-law-info-again","showagain_x_position":"100px","text":"#000","show_once_yn":false,"show_once":"10000","logging_on":false,"as_popup":false,"popup_overlay":true,"bar_heading_text":"","cookie_bar_as":"banner","popup_showagain_position":"bottom-right","widget_position":"left"}';
    /* ]]> */
</script>		<script>
    ( function ( body ) {
        'use strict';
        body.className = body.className.replace( /\btribe-no-js\b/, 'tribe-js' );
    } )( document.body );
</script>
<script> /* <![CDATA[ */var tribe_l10n_datatables = {"aria":{"sort_ascending":": activate to sort column ascending","sort_descending":": activate to sort column descending"},"length_menu":"Show _MENU_ entries","empty_table":"No data available in table","info":"Showing _START_ to _END_ of _TOTAL_ entries","info_empty":"Showing 0 to 0 of 0 entries","info_filtered":"(filtered from _MAX_ total entries)","zero_records":"No matching records found","search":"Search:","all_selected_text":"All items on this page were selected. ","select_all_link":"Select all pages","clear_selection":"Clear Selection.","pagination":{"all":"All","next":"Next","previous":"Previous"},"select":{"rows":{"0":"","_":": Selected %d rows","1":": Selected 1 row"}},"datepicker":{"dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesMin":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Prev","currentText":"Today","closeText":"Done","today":"Today","clear":"Clear"}};var tribe_system_info = {"sysinfo_optin_nonce":"bf60c75514","clipboard_btn_text":"Copy to clipboard","clipboard_copied_text":"System info copied","clipboard_fail_text":"Press \"Cmd + C\" to copy"};/* ]]> */ </script>	<script type="text/javascript">
    var c = document.body.className;
    c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
    document.body.className = c;
</script>
<link rel='stylesheet' id='mailpoet_public-css'  href='/wp-content/plugins/mailpoet/assets/css/public.cae357df.css?ver=5.0.4' type='text/css' media='all' />
<script type='text/javascript' src='/wp-content/plugins/cmsmasters-mega-menu/js/jquery.megaMenu.js?ver=1.2.9'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
    /* ]]> */
</script>
<script type='text/javascript' src='/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.1'></script>
<script type='text/javascript' src='/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"cart\/","is_cart":"","cart_redirect_after_add":"no"};
    /* ]]> */
</script>
<script type='text/javascript' src='/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=3.5.3'></script>
<script type='text/javascript' src='/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4'></script>

<script type='text/javascript' src='/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=3.5.3'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_3c16cca33cdf5149ebe2febe54fabb8c","fragment_name":"wc_fragments_3c16cca33cdf5149ebe2febe54fabb8c"};
    /* ]]> */
</script>

<script type='text/javascript' src='/wp-content/themes/good-food/js/cmsmasters-hover-slider.min.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-content/themes/good-food/js/easing.min.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-content/themes/good-food/js/easy-pie-chart.min.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-content/themes/good-food/js/mousewheel.min.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-content/themes/good-food/js/owlcarousel.min.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-includes/js/imagesloaded.min.js?ver=3.2.0'></script>
<script type='text/javascript' src='/wp-content/themes/good-food/js/request-animation-frame.min.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-content/themes/good-food/js/scrollspy.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-content/themes/good-food/js/scroll-to.min.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-content/themes/good-food/js/stellar.min.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-content/themes/good-food/js/waypoints.min.js?ver=1.0.0'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var cmsmasters_script = {"theme_url":"\/wp-content\/themes\/good-food","site_url":"","ajaxurl":"\/wp-admin\/admin-ajax.php","nonce_ajax_like":"65f59d53a4","nonce_ajax_view":"e794a6ef2a","project_puzzle_proportion":"1","gmap_api_key":"AIzaSyBz2LZYZ7NgCSc7JlVIDUADZ-aSw1mdDsY","gmap_api_key_notice":"Please add your Google Maps API key","gmap_api_key_notice_link":"read more how","primary_color":"#ff7055","ilightbox_skin":"dark","ilightbox_path":"vertical","ilightbox_infinite":"0","ilightbox_aspect_ratio":"1","ilightbox_mobile_optimizer":"1","ilightbox_max_scale":"1","ilightbox_min_scale":"0.2","ilightbox_inner_toolbar":"0","ilightbox_smart_recognition":"0","ilightbox_fullscreen_one_slide":"0","ilightbox_fullscreen_viewport":"center","ilightbox_controls_toolbar":"1","ilightbox_controls_arrows":"0","ilightbox_controls_fullscreen":"1","ilightbox_controls_thumbnail":"1","ilightbox_controls_keyboard":"1","ilightbox_controls_mousewheel":"1","ilightbox_controls_swipe":"1","ilightbox_controls_slideshow":"0","ilightbox_close_text":"Close","ilightbox_enter_fullscreen_text":"Enter Fullscreen (Shift+Enter)","ilightbox_exit_fullscreen_text":"Exit Fullscreen (Shift+Enter)","ilightbox_slideshow_text":"Slideshow","ilightbox_next_text":"Next","ilightbox_previous_text":"Previous","ilightbox_load_image_error":"An error occurred when trying to load photo.","ilightbox_load_contents_error":"An error occurred when trying to load contents.","ilightbox_missing_plugin_error":"The content your are attempting to view requires the <a href='{pluginspage}' target='_blank'>{type} plugin<\\\/a>."};
    /* ]]> */
</script>
<script type='text/javascript' src='/wp-content/themes/good-food/js/jquery.script.js?ver=1.0.0'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var cmsmasters_theme_script = {"primary_color":"#ff7055"};
    /* ]]> */
</script>
<script type='text/javascript' src='/wp-content/themes/good-food/theme-framework/theme-style/js/jquery.theme-script.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-content/themes/good-food/js/jquery.tweet.min.js?ver=1.3.1'></script>
<script type='text/javascript' src='/wp-content/themes/good-food/js/smooth-sticky.min.js?ver=1.0.2'></script>
<script type='text/javascript' src='/wp-content/themes/good-food/js/jquery.isotope.min.js?ver=1.5.19'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var cmsmasters_isotope_mode = {"theme_url":"wp-content\/themes\/good-food","site_url":"","ajaxurl":"wp-admin\/admin-ajax.php","nonce_ajax_like":"65f59d53a4","nonce_ajax_view":"e794a6ef2a","project_puzzle_proportion":"1","gmap_api_key":"AIzaSyBz2LZYZ7NgCSc7JlVIDUADZ-aSw1mdDsY","gmap_api_key_notice":"Please add your Google Maps API key","gmap_api_key_notice_link":"read more how","primary_color":"#ff7055","ilightbox_skin":"dark","ilightbox_path":"vertical","ilightbox_infinite":"0","ilightbox_aspect_ratio":"1","ilightbox_mobile_optimizer":"1","ilightbox_max_scale":"1","ilightbox_min_scale":"0.2","ilightbox_inner_toolbar":"0","ilightbox_smart_recognition":"0","ilightbox_fullscreen_one_slide":"0","ilightbox_fullscreen_viewport":"center","ilightbox_controls_toolbar":"1","ilightbox_controls_arrows":"0","ilightbox_controls_fullscreen":"1","ilightbox_controls_thumbnail":"1","ilightbox_controls_keyboard":"1","ilightbox_controls_mousewheel":"1","ilightbox_controls_swipe":"1","ilightbox_controls_slideshow":"0","ilightbox_close_text":"Close","ilightbox_enter_fullscreen_text":"Enter Fullscreen (Shift+Enter)","ilightbox_exit_fullscreen_text":"Exit Fullscreen (Shift+Enter)","ilightbox_slideshow_text":"Slideshow","ilightbox_next_text":"Next","ilightbox_previous_text":"Previous","ilightbox_load_image_error":"An error occurred when trying to load photo.","ilightbox_load_contents_error":"An error occurred when trying to load contents.","ilightbox_missing_plugin_error":"The content your are attempting to view requires the <a href='{pluginspage}' target='_blank'>{type} plugin<\\\/a>."};
    /* ]]> */
</script>
<script type='text/javascript' src='/wp-content/themes/good-food/theme-framework/theme-style/js/jquery.isotope.mode.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-includes/js/comment-reply.min.js?ver=5.0.4'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var cmsmasters_woo_script = {"currency_symbol":"\u00a3","thumbnail_image_width":"60","thumbnail_image_height":"60"};
    /* ]]> */
</script>
<script type='text/javascript' src='/wp-content/themes/good-food/woocommerce/cmsmasters-framework/theme-style/js/jquery.plugin-script.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-includes/js/wp-embed.min.js?ver=5.0.4'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var ratingsL10n = {"plugin_url":"wp-content\/plugins\/wp-postratings","ajax_url":"wp-admin\/admin-ajax.php","text_wait":"Please rate only 1 item at a time.","image":"stars(png)","image_ext":"png","max":"5","show_loading":"1","show_fading":"1","custom":"0"};
    var ratings_mouseover_image=new Image();ratings_mouseover_image.src="wp-content/plugins/wp-postratings/images/stars(png)/rating_over.png";;
    var ratingsL10n = {"plugin_url":"wp-content\/themes\/good-food\/cmsmasters-wp-postratings\/cmsmasters-framework\/theme-style","ajax_url":"wp-admin\/admin-ajax.php","text_wait":"Please rate only 1 post at a time.","image":"stars(png)","image_ext":"png","max":"5","show_loading":"1","show_fading":"1","custom":"0"};
    var ratings_mouseover_image=new Image();ratings_mouseover_image.src=ratingsL10n.plugin_url+"/images/"+ratingsL10n.image+"/rating_over."+ratingsL10n.image_ext;;
    var ratingsL10n = {"plugin_url":"wp-content\/themes\/good-food\/cmsmasters-wp-postratings\/cmsmasters-framework\/theme-style","ajax_url":"wp-admin\/admin-ajax.php","text_wait":"Please rate only 1 post at a time.","image":"stars(png)","image_ext":"png","max":"5","show_loading":"1","show_fading":"1","custom":"0"};
    var ratings_mouseover_image=new Image();ratings_mouseover_image.src=ratingsL10n.plugin_url+"/images/"+ratingsL10n.image+"/rating_over."+ratingsL10n.image_ext;;
    /* ]]> */
</script>
<script type='text/javascript' src='/wp-content/plugins/wp-postratings/js/postratings-js.js?ver=1.86.1'></script>
<script type='text/javascript' src='/wp-content/plugins/mailpoet/assets/js/vendor.169f324b.js?ver=3.17.1'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var MailPoetForm = {"ajax_url":"wp-admin\/admin-ajax.php","is_rtl":""};
    /* ]]> */
</script>
<script type='text/javascript' src='/wp-content/plugins/mailpoet/assets/js/public.3cbea7a9.js?ver=3.17.1'></script>
<script type='text/javascript'>
    function initMailpoetTranslation() {
        if(typeof MailPoet !== 'undefined') {
            MailPoet.I18n.add('ajaxFailedErrorMessage', 'An error has happened while performing a request, please try again later.')
        } else {
            setTimeout(initMailpoetTranslation, 250);
        }
    }
    setTimeout(initMailpoetTranslation, 250);
</script>
</body>
</html>
