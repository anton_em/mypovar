<!DOCTYPE html>
<html lang="en-US" class="cmsmasters_html">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="profile" href="https://gmpg.org/xfn/11" />
    <link rel="pingback" href="xmlrpc.php" />
    <title>[title]</title>
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.0.4"}};
        !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='layerslider-css'  href='/wp-content/plugins/LayerSlider/static/layerslider/css/layerslider.css?ver=6.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='wp-block-library-css'  href='/wp-includes/css/dist/block-library/style.min.css?ver=5.0.4' type='text/css' media='all' />
    <link rel='stylesheet' id='contact-form-7-css'  href='/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='cookie-law-info-css'  href='/wp-content/plugins/cookie-law-info/public/css/cookie-law-info-public.css?ver=1.7.3' type='text/css' media='all' />
    <link rel='stylesheet' id='cookie-law-info-gdpr-css'  href='/wp-content/plugins/cookie-law-info/public/css/cookie-law-info-gdpr.css?ver=1.7.3' type='text/css' media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css'  href='/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8.1' type='text/css' media='all' />
    <style id='rs-plugin-settings-inline-css' type='text/css'>
        #rs-demo-id {}
    </style>
    <style id='woocommerce-inline-inline-css' type='text/css'>
        .woocommerce form .form-row .required { visibility: visible; }
    </style>
    <link rel='stylesheet' id='wp-postratings-css'  href='/wp-content/plugins/wp-postratings/css/postratings-css.css?ver=1.86.1' type='text/css' media='all' />
    <link rel='stylesheet' id='good-food-theme-style-css'  href='/wp-content/themes/good-food/style.css?ver=1.0.0' type='text/css' media='screen, print' />
    <link rel='stylesheet' id='good-food-style-css'  href='/wp-content/themes/good-food/theme-framework/theme-style/css/style.css?ver=1.0.0' type='text/css' media='screen, print' />
    <style id='good-food-style-inline-css' type='text/css'>

        html body {
            background-color : #ffffff;
            background-image : url(/wp-content/uploads/2017/07/background.jpg);
            background-position : top left;
            background-repeat : repeat;
            background-attachment : scroll;
            background-size : auto;

        }

        .headline_aligner,
        .cmsmasters_breadcrumbs_aligner {
            min-height:170px;
        }



        #page .cmsmasters_social_icon_color.cmsmasters_social_icon_1 {
            color:#2c3b4b;
        }


        #page .cmsmasters_social_icon_color.cmsmasters_social_icon_1:hover {
            color:#8e9092;
        }

        #page .cmsmasters_social_icon_color.cmsmasters_social_icon_2 {
            color:#2c3b4b;
        }


        #page .cmsmasters_social_icon_color.cmsmasters_social_icon_2:hover {
            color:#8e9092;
        }

        #page .cmsmasters_social_icon_color.cmsmasters_social_icon_3 {
            color:#2c3b4b;
        }


        #page .cmsmasters_social_icon_color.cmsmasters_social_icon_3:hover {
            color:#8e9092;
        }

        #page .cmsmasters_social_icon_color.cmsmasters_social_icon_4 {
            color:#2c3b4b;
        }


        #page .cmsmasters_social_icon_color.cmsmasters_social_icon_4:hover {
            color:#8e9092;
        }

        #page .cmsmasters_social_icon_color.cmsmasters_social_icon_5 {
            color:#2c3b4b;
        }


        #page .cmsmasters_social_icon_color.cmsmasters_social_icon_5:hover {
            color:#8e9092;
        }

        .header_top {
            height : 38px;
        }

        .header_mid {
            height : 65px;
        }

        .header_bot {
            height : 210px;
        }

        #page.cmsmasters_heading_after_header #middle,
        #page.cmsmasters_heading_under_header #middle .headline .headline_outer {
            padding-top : 65px;
        }

        #page.cmsmasters_heading_after_header.enable_header_top #middle,
        #page.cmsmasters_heading_under_header.enable_header_top #middle .headline .headline_outer {
            padding-top : 103px;
        }

        #page.cmsmasters_heading_after_header.enable_header_bottom #middle,
        #page.cmsmasters_heading_under_header.enable_header_bottom #middle .headline .headline_outer {
            padding-top : 275px;
        }

        #page.cmsmasters_heading_after_header.enable_header_top.enable_header_bottom #middle,
        #page.cmsmasters_heading_under_header.enable_header_top.enable_header_bottom #middle .headline .headline_outer {
            padding-top : 313px;
        }

        @media only screen and (max-width: 1024px) {
            .header_top,
            .header_mid,
            .header_bot {
                height : auto;
            }

            .header_mid .header_mid_inner .header_mid_inner_cont > div {
                height : 130px;
            }

            #page.cmsmasters_heading_after_header #middle,
            #page.cmsmasters_heading_under_header #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_top #middle,
            #page.cmsmasters_heading_under_header.enable_header_top #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_bottom #middle,
            #page.cmsmasters_heading_under_header.enable_header_bottom #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_top.enable_header_bottom #middle,
            #page.cmsmasters_heading_under_header.enable_header_top.enable_header_bottom #middle .headline .headline_outer {
                padding-top : 0 !important;
            }
        }

        @media only screen and (max-width: 768px) {
            .header_mid .header_mid_inner .header_mid_inner_cont > div {
                height : 65px;
            }
        }


        .header_mid .header_mid_inner .logo_wrap,
        .header_bot .header_bot_inner .logo_wrap {
            width : 380px;
        }

        .header_mid_inner .logo .logo_retina,
        .header_bot_inner .logo .logo_retina {
            width : 380px;
            max-width : 380px;
        }

    </style>
    <link rel='stylesheet' id='good-food-adaptive-css'  href='/wp-content/themes/good-food/theme-framework/theme-style/css/adaptive.css?ver=1.0.0' type='text/css' media='screen, print' />
    <link rel='stylesheet' id='good-food-retina-css'  href='/wp-content/themes/good-food/theme-framework/theme-style/css/retina.css?ver=1.0.0' type='text/css' media='screen' />
    <style id='good-food-retina-inline-css' type='text/css'>
        #cmsmasters_row_1nq1g9fbyt .cmsmasters_row_outer_parent {
            padding-top: 20px;
        }

        #cmsmasters_row_1nq1g9fbyt .cmsmasters_row_outer_parent {
            padding-bottom: 0px;
        }


        #cmsmasters_column_g7af8pgtav {
            border-style:default;
        }


        #cmsmasters_heading_9vr4xbdakl {
            text-align:center;
            margin-top:0px;
            margin-bottom:40px;
        }

        #cmsmasters_heading_9vr4xbdakl .cmsmasters_heading {
            text-align:center;
        }

        #cmsmasters_heading_9vr4xbdakl .cmsmasters_heading, #cmsmasters_heading_9vr4xbdakl .cmsmasters_heading a {
            font-size:45px;
            line-height:60px;
        }

        #cmsmasters_heading_9vr4xbdakl .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_9vr4xbdakl .cmsmasters_heading_divider {
        }



        #cmsmasters_divider_24al8dhwhq {
            border-bottom-width:4px;
            border-bottom-style:solid;
            margin-top:0px;
            margin-bottom:0px;
            border-bottom-color:#2c3b4b;
        }
        #cmsmasters_row_nqxpnixor .cmsmasters_row_outer_parent {
            padding-top: 70px;
        }

        #cmsmasters_row_nqxpnixor .cmsmasters_row_outer_parent {
            padding-bottom: 0px;
        }


        @media only screen and (max-width: 768px) {
            #cmsmasters_row_nqxpnixor .cmsmasters_row_outer_parent {
                padding-top: 20px;
            }
        }

        @media only screen and (max-width: 540px) {
            #cmsmasters_row_nqxpnixor .cmsmasters_row_outer_parent {
                padding-top: 20px;
            }
        }

        @media only screen and (max-width: 320px) {
            #cmsmasters_row_nqxpnixor .cmsmasters_row_outer_parent {
                padding-top: 20px;
            }
        }

        #cmsmasters_column_mcvqxars8s {
            border-style:default;
        }


        #cmsmasters_fb_5ljq5091 {
            padding-top:20px;
            padding-bottom:0px;
            background-color:rgba(255,255,255,0);
        }

        #cmsmasters_fb_5ljq5091 .featured_block_inner {
            width: 100%;
            padding: ;
            text-align: center;
            margin:0 auto;
        }

        #cmsmasters_fb_5ljq5091 .featured_block_text {
            text-align: center;
        }



        #cmsmasters_heading_sr958mosp {
            text-align:center;
            margin-top:0px;
            margin-bottom:0px;
        }

        #cmsmasters_heading_sr958mosp .cmsmasters_heading {
            text-align:center;
        }

        #cmsmasters_heading_sr958mosp .cmsmasters_heading, #cmsmasters_heading_sr958mosp .cmsmasters_heading a {
            font-size:25px;
            line-height:30px;
        }

        #cmsmasters_heading_sr958mosp .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_sr958mosp .cmsmasters_heading_divider {
        }



        #cmsmasters_heading_qswstud0sa {
            text-align:center;
            margin-top:0px;
            margin-bottom:0px;
        }

        #cmsmasters_heading_qswstud0sa .cmsmasters_heading {
            text-align:center;
        }

        #cmsmasters_heading_qswstud0sa .cmsmasters_heading, #cmsmasters_heading_qswstud0sa .cmsmasters_heading a {
            font-size:18px;
            line-height:30px;
            color:#8e9092;
        }

        #cmsmasters_heading_qswstud0sa .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_qswstud0sa .cmsmasters_heading_divider {
        }



        #cmsmasters_column_z0jym1qjff {
            border-style:default;
        }


        #cmsmasters_fb_4iysws4bdh {
            padding-top:5px;
            padding-bottom:25px;
            background-color:rgba(255,255,255,0);
            background-image: url(wp-content/uploads/2016/06/line.jpg);
            background-position: top left;
            background-repeat: repeat-y;
            background-attachment: scroll;
            background-size: auto;
        }

        #cmsmasters_fb_4iysws4bdh .featured_block_inner {
            width: 90%;
            padding: ;
            text-align: center;
            margin:0 auto;
        }

        #cmsmasters_fb_4iysws4bdh .featured_block_text {
            text-align: center;
        }


        .cmsmasters_sidebar .widget_search {
            padding-bottom:0px;
        }

        @media only screen and (max-width: 768px) {
            #page .cmsmasters_recipes_custom_blocks .cmsmasters_featured_block {
                background:none;
            }

            #page .cmsmasters_recipes_custom_blocks .cmsmasters_featured_block .featured_block_inner {
                padding-left:0;
                padding-right:0;
            }
        }
        #cmsmasters_column_uchnjyjmnr {
            border-style:default;
        }


        #cmsmasters_fb_0x2n85yne {
            padding-top:0px;
            padding-bottom:0px;
            background-color:rgba(255,255,255,0);
            background-image: url(wp-content/uploads/2016/06/line.jpg);
            background-position: top left;
            background-repeat: repeat-y;
            background-attachment: scroll;
            background-size: auto;
        }

        #cmsmasters_fb_0x2n85yne .featured_block_inner {
            width: 100%;
            padding: 40px 30px 20px 60px;
            text-align: center;
            margin:0 auto;
        }

        #cmsmasters_fb_0x2n85yne .featured_block_text {
            text-align: center;
        }



        #cmsmasters_heading_cfo3boswfi {
            text-align:left;
            margin-top:5px;
            margin-bottom:0px;
        }

        #cmsmasters_heading_cfo3boswfi .cmsmasters_heading {
            text-align:left;
        }

        #cmsmasters_heading_cfo3boswfi .cmsmasters_heading, #cmsmasters_heading_cfo3boswfi .cmsmasters_heading a {
            font-size:14px;
            color:#8e9092;
        }

        #cmsmasters_heading_cfo3boswfi .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_cfo3boswfi .cmsmasters_heading_divider {
        }


        #cmsmasters_row_x61kquuw89 .cmsmasters_row_outer_parent {
            padding-top: 85px;
        }

        #cmsmasters_row_x61kquuw89 .cmsmasters_row_outer_parent {
            padding-bottom: 0px;
        }


        #cmsmasters_column_4yqgz49ri {
            border-style:default;
        }


        #cmsmasters_heading_50200ddntl {
            text-align:center;
            margin-top:0px;
            margin-bottom:60px;
        }

        #cmsmasters_heading_50200ddntl .cmsmasters_heading {
            text-align:center;
        }

        #cmsmasters_heading_50200ddntl .cmsmasters_heading, #cmsmasters_heading_50200ddntl .cmsmasters_heading a {
            color:#fffef7;
        }

        #cmsmasters_heading_50200ddntl .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_50200ddntl .cmsmasters_heading_divider {
        }



        #cmsmasters_divider_ofa4n8uv5f {
            border-bottom-width:0px;
            border-bottom-style:solid;
            margin-top:0px;
            margin-bottom:-10px;
        }

        #cmsmasters_button_4ls0p85qpf {
            text-align:center;
        }

        #cmsmasters_button_4ls0p85qpf .cmsmasters_button:before {
            margin-right:.5em;
            margin-left:0;
            vertical-align:baseline;
        }

        #cmsmasters_button_4ls0p85qpf .cmsmasters_button {
            font-weight:default;
            font-style:default;
            text-transform:default;
            padding-right:40px;
            padding-left:40px;
            border-style:default;
            background-color:#2c3b4b;
            color:#ffffff;
            border-color:#2c3b4b;
        }
        #cmsmasters_button_4ls0p85qpf .cmsmasters_button:hover {
            background-color:#ff7055;
            color:#ffffff;
            border-color:#ff7055;
        }

        #cmsmasters_row_j492462pce .cmsmasters_row_outer_parent {
            padding-top: 90px;
        }

        #cmsmasters_row_j492462pce .cmsmasters_row_outer_parent {
            padding-bottom: 85px;
        }


        #cmsmasters_column_0d0f4a91bc {
            border-style:default;
            background-image: url(wp-content/uploads/2016/06/2.jpg);
            background-position: center right;
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-size: cover;
        }

        #cmsmasters_column_0d0f4a91bc .cmsmasters_column_inner {
            padding: 60px 43% 64px 60px;
        }


        @media only screen and (max-width: 1024px) {
            #cmsmasters_column_0d0f4a91bc .cmsmasters_column_inner {
                padding: 60px 6% 60px 60px;
            }
        }

        @media only screen and (max-width: 768px) {
            #cmsmasters_column_0d0f4a91bc .cmsmasters_column_inner {
                padding: 60px 6% 60px 60px;
            }
        }

        @media only screen and (max-width: 540px) {
            #cmsmasters_column_0d0f4a91bc .cmsmasters_column_inner {
                padding: 30px 3% 30px 30px;
            }
        }

        @media only screen and (max-width: 320px) {
            #cmsmasters_column_0d0f4a91bc .cmsmasters_column_inner {
                padding: 30px 3% 30px 30px;
            }
        }
        @media only screen and (max-width: 1024px) {
            .cmsmasters_recipes_custom_fb .cmsmasters_row_columns_behavior .cmsmasters_column {
                margin-bottom:20px;
            }
        }
        #cmsmasters_divider_ryo5vvcgl7 {
            border-bottom-width:4px;
            border-bottom-style:solid;
            margin-top:0px;
            margin-bottom:12px;
            border-bottom-color:rgba(255,255,255,0.3);
        }

        #cmsmasters_heading_u5lta4u2ga {
            text-align:left;
            margin-top:0px;
            margin-bottom:10px;
        }

        #cmsmasters_heading_u5lta4u2ga .cmsmasters_heading {
            text-align:left;
        }

        #cmsmasters_heading_u5lta4u2ga .cmsmasters_heading, #cmsmasters_heading_u5lta4u2ga .cmsmasters_heading a {
            font-size:24px;
            line-height:32px;
            color:#ffffff;
        }

        #cmsmasters_heading_u5lta4u2ga .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_u5lta4u2ga .cmsmasters_heading_divider {
        }



        #cmsmasters_divider_zq86vkoas {
            border-bottom-width:1px;
            border-bottom-style:solid;
            margin-top:0px;
            margin-bottom:20px;
            border-bottom-color:rgba(255,255,255,0.3);
        }

        #cmsmasters_heading_omgndniakg {
            text-align:left;
            margin-top:0px;
            margin-bottom:35px;
        }

        #cmsmasters_heading_omgndniakg .cmsmasters_heading {
            text-align:left;
        }

        #cmsmasters_heading_omgndniakg .cmsmasters_heading, #cmsmasters_heading_omgndniakg .cmsmasters_heading a {
            font-size:20px;
            line-height:26px;
            color:#ffffff;
        }

        #cmsmasters_heading_omgndniakg .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_omgndniakg .cmsmasters_heading_divider {
        }



        #cmsmasters_button_i8rmwo0le {
            float:left;
        }

        #cmsmasters_button_i8rmwo0le .cmsmasters_button:before {
            margin-right:.5em;
            margin-left:0;
            vertical-align:baseline;
        }

        #cmsmasters_button_i8rmwo0le .cmsmasters_button {
            font-weight:default;
            font-style:default;
            text-transform:default;
            padding-right:30px;
            padding-left:30px;
            border-style:default;
            background-color:#ff7055;
            color:#ffffff;
            border-color:#ff7055;
        }
        #cmsmasters_button_i8rmwo0le .cmsmasters_button:hover {
            background-color:#ffffff;
            color:#ff7055;
            border-color:#ffffff;
        }


        #cmsmasters_column_af739082c0 {
            border-style:default;
            background-image: url(wp-content/uploads/2015/04/3-1.jpg);
            background-position: center right;
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-size: cover;
        }

        #cmsmasters_column_af739082c0 .cmsmasters_column_inner {
            padding: 60px 43% 64px 60px;
        }


        @media only screen and (max-width: 1024px) {
            #cmsmasters_column_af739082c0 .cmsmasters_column_inner {
                padding: 60px 6% 60px 60px;
            }
        }

        @media only screen and (max-width: 768px) {
            #cmsmasters_column_af739082c0 .cmsmasters_column_inner {
                padding: 60px 6% 60px 60px;
            }
        }

        @media only screen and (max-width: 540px) {
            #cmsmasters_column_af739082c0 .cmsmasters_column_inner {
                padding: 30px 3% 30px 30px;
            }
        }

        @media only screen and (max-width: 320px) {
            #cmsmasters_column_af739082c0 .cmsmasters_column_inner {
                padding: 30px 3% 30px 30px;
            }
        }

        #cmsmasters_divider_lxb01044n {
            border-bottom-width:4px;
            border-bottom-style:solid;
            margin-top:0px;
            margin-bottom:12px;
            border-bottom-color:rgba(255,255,255,0.3);
        }

        #cmsmasters_heading_1qbiouue25 {
            text-align:left;
            margin-top:0px;
            margin-bottom:10px;
        }

        #cmsmasters_heading_1qbiouue25 .cmsmasters_heading {
            text-align:left;
        }

        #cmsmasters_heading_1qbiouue25 .cmsmasters_heading, #cmsmasters_heading_1qbiouue25 .cmsmasters_heading a {
            font-size:24px;
            line-height:32px;
            color:#ffffff;
        }

        #cmsmasters_heading_1qbiouue25 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_1qbiouue25 .cmsmasters_heading_divider {
        }



        #cmsmasters_divider_wjmz2hmgr {
            border-bottom-width:1px;
            border-bottom-style:solid;
            margin-top:0px;
            margin-bottom:20px;
            border-bottom-color:rgba(255,255,255,0.3);
        }

        #cmsmasters_heading_gu1d9fs7o {
            text-align:left;
            margin-top:0px;
            margin-bottom:35px;
        }

        #cmsmasters_heading_gu1d9fs7o .cmsmasters_heading {
            text-align:left;
        }

        #cmsmasters_heading_gu1d9fs7o .cmsmasters_heading, #cmsmasters_heading_gu1d9fs7o .cmsmasters_heading a {
            font-size:20px;
            line-height:26px;
            color:#ffffff;
        }

        #cmsmasters_heading_gu1d9fs7o .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_gu1d9fs7o .cmsmasters_heading_divider {
        }



        #cmsmasters_button_2jx8m2rwr {
            float:left;
        }

        #cmsmasters_button_2jx8m2rwr .cmsmasters_button:before {
            margin-right:.5em;
            margin-left:0;
            vertical-align:baseline;
        }

        #cmsmasters_button_2jx8m2rwr .cmsmasters_button {
            font-weight:default;
            font-style:default;
            text-transform:default;
            padding-right:30px;
            padding-left:30px;
            border-style:default;
            background-color:#ff7055;
            color:#ffffff;
            border-color:#ff7055;
        }
        #cmsmasters_button_2jx8m2rwr .cmsmasters_button:hover {
            background-color:#ffffff;
            color:#ff7055;
            border-color:#ffffff;
        }

        #cmsmasters_row_buyd8jdis .cmsmasters_row_outer_parent {
            padding-top: 0px;
        }

        #cmsmasters_row_buyd8jdis .cmsmasters_row_outer_parent {
            padding-bottom: 5px;
        }


        #cmsmasters_column_4ktfd8idu4 {
            border-style:default;
        }


        #cmsmasters_heading_pov49ywg7j {
            text-align:center;
            margin-top:0px;
            margin-bottom:35px;
        }

        #cmsmasters_heading_pov49ywg7j .cmsmasters_heading {
            text-align:center;
        }

        #cmsmasters_heading_pov49ywg7j .cmsmasters_heading, #cmsmasters_heading_pov49ywg7j .cmsmasters_heading a {
            font-size:46px;
        }

        #cmsmasters_heading_pov49ywg7j .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_pov49ywg7j .cmsmasters_heading_divider {
        }



        #cmsmasters_heading_hgxaf20me {
            text-align:center;
            margin-top:0px;
            margin-bottom:60px;
        }

        #cmsmasters_heading_hgxaf20me .cmsmasters_heading {
            text-align:center;
        }

        #cmsmasters_heading_hgxaf20me .cmsmasters_heading, #cmsmasters_heading_hgxaf20me .cmsmasters_heading a {
            color:#fffef7;
        }

        #cmsmasters_heading_hgxaf20me .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_hgxaf20me .cmsmasters_heading_divider {
        }


        #cmsmasters_row_rhx1xftan .cmsmasters_row_outer_parent {
            padding-top: 0px;
        }

        #cmsmasters_row_rhx1xftan .cmsmasters_row_outer_parent {
            padding-bottom: 70px;
        }


        #cmsmasters_column_06spw7z62 {
            border-style:default;
            background-image: url(wp-content/uploads/2016/06/1.jpg);
            background-position: top center;
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-size: cover;
        }

        #cmsmasters_column_06spw7z62 .cmsmasters_column_inner {
            padding: 30% 17% 33% 14%;
        }


        @media only screen and (max-width: 1024px) {
            #cmsmasters_column_06spw7z62 .cmsmasters_column_inner {
                padding: 30% 17% 22% 14%;
            }
        }
        #page .cmsmasters_recipes_and_pricing_custom .cmsmasters_column_inner .cmsmasters_pricing_table {
            margin:0;
        }

        #page .cmsmasters_recipes_and_pricing_custom .cmsmasters_column_inner .cmsmasters_pricing_item {
            padding:0 0 15px;
        }
        #cmsmasters_heading_jvkj8m3sl {
            text-align:left;
            margin-top:0px;
            margin-bottom:15px;
        }

        #cmsmasters_heading_jvkj8m3sl .cmsmasters_heading {
            text-align:left;
        }

        #cmsmasters_heading_jvkj8m3sl .cmsmasters_heading, #cmsmasters_heading_jvkj8m3sl .cmsmasters_heading a {
            font-size:30px;
            line-height:36px;
            color:#ffffff;
        }

        #cmsmasters_heading_jvkj8m3sl .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_jvkj8m3sl .cmsmasters_heading_divider {
        }



        #cmsmasters_divider_tao1h3iq69 {
            border-bottom-width:2px;
            border-bottom-style:solid;
            margin-top:0px;
            margin-bottom:30px;
            border-bottom-color:#ffffff;
        }

        #cmsmasters_heading_hor9gfqbzf {
            text-align:left;
            margin-top:0px;
            margin-bottom:35px;
        }

        #cmsmasters_heading_hor9gfqbzf .cmsmasters_heading {
            text-align:left;
        }

        #cmsmasters_heading_hor9gfqbzf .cmsmasters_heading, #cmsmasters_heading_hor9gfqbzf .cmsmasters_heading a {
            font-size:20px;
            line-height:26px;
            color:#ffffff;
        }

        #cmsmasters_heading_hor9gfqbzf .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_hor9gfqbzf .cmsmasters_heading_divider {
        }



        #cmsmasters_button_u1f3wruub {
            float:left;
        }

        #cmsmasters_button_u1f3wruub .cmsmasters_button:before {
            margin-right:.5em;
            margin-left:0;
            vertical-align:baseline;
        }

        #cmsmasters_button_u1f3wruub .cmsmasters_button {
            font-weight:default;
            font-style:default;
            text-transform:default;
            padding-right:30px;
            padding-left:30px;
            border-style:default;
        }
        #cmsmasters_button_u1f3wruub .cmsmasters_button:hover {
        }


        #cmsmasters_pricing_item_ipsr217g5e .cmsmasters_button:before {
            margin-right:.5em;
            margin-left:0;
            vertical-align:baseline;
        }

        #cmsmasters_pricing_item_ipsr217g5e .cmsmasters_button {
            font-weight:normal;
            font-style:default;
            padding-right:30px;
            padding-left:30px;
            border-style:solid;
        }
        #cmsmasters_pricing_item_ipsr217g5e .cmsmasters_button:hover {
        }

        #cmsmasters_pricing_item_52p6hjsthn .cmsmasters_button:before {
            margin-right:.5em;
            margin-left:0;
            vertical-align:baseline;
        }

        #cmsmasters_pricing_item_52p6hjsthn .cmsmasters_button {
            font-weight:normal;
            font-style:default;
            padding-right:30px;
            padding-left:30px;
            border-style:solid;
        }
        #cmsmasters_pricing_item_52p6hjsthn .cmsmasters_button:hover {
        }

        #cmsmasters_pricing_item_5wn1tcmrm .cmsmasters_button:before {
            margin-right:.5em;
            margin-left:0;
            vertical-align:baseline;
        }

        #cmsmasters_pricing_item_5wn1tcmrm .cmsmasters_button {
            font-weight:normal;
            font-style:default;
            padding-right:30px;
            padding-left:30px;
            border-style:solid;
        }
        #cmsmasters_pricing_item_5wn1tcmrm .cmsmasters_button:hover {
        }
        #cmsmasters_row_28ftb6c6cl .cmsmasters_row_outer_parent {
            padding-top: 0px;
        }

        #cmsmasters_row_28ftb6c6cl .cmsmasters_row_outer_parent {
            padding-bottom: 0px;
        }


        #cmsmasters_column_514ip0rnzr {
            border-style:default;
        }

        #page .cmsmasters_recipes_and_pricing_custom .cmsmasters_column_inner .cmsmasters_pricing_table {
            margin:0;
        }

        #page .cmsmasters_recipes_and_pricing_custom .cmsmasters_column_inner .cmsmasters_pricing_item {
            padding:0 0 15px;
        }#cmsmasters_row_utfeotnfyc .cmsmasters_row_outer_parent {
             padding-top: 0px;
         }

        #cmsmasters_row_utfeotnfyc .cmsmasters_row_outer_parent {
            padding-bottom: 95px;
        }


        #cmsmasters_column_pmz8qms108 {
            border-style:default;
        }


        #cmsmasters_heading_1vui6lf2g {
            text-align:center;
            margin-top:0px;
            margin-bottom:35px;
        }

        #cmsmasters_heading_1vui6lf2g .cmsmasters_heading {
            text-align:center;
        }

        #cmsmasters_heading_1vui6lf2g .cmsmasters_heading, #cmsmasters_heading_1vui6lf2g .cmsmasters_heading a {
            font-size:46px;
            font-style:inherit;
        }

        #cmsmasters_heading_1vui6lf2g .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_1vui6lf2g .cmsmasters_heading_divider {
        }



        #cmsmasters_heading_fsx702c6wa {
            text-align:center;
            margin-top:0px;
            margin-bottom:60px;
        }

        #cmsmasters_heading_fsx702c6wa .cmsmasters_heading {
            text-align:center;
        }

        #cmsmasters_heading_fsx702c6wa .cmsmasters_heading, #cmsmasters_heading_fsx702c6wa .cmsmasters_heading a {
            color:#fffef7;
        }

        #cmsmasters_heading_fsx702c6wa .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_fsx702c6wa .cmsmasters_heading_divider {
        }



        #cmsmasters_divider_k9y2y2p6qe {
            border-bottom-width:1px;
            border-bottom-style:solid;
            margin-top:0px;
            margin-bottom:30px;
            border-bottom-color:#dcd9c9;
        }

        #cmsmasters_divider_403pfbwjm {
            border-bottom-width:1px;
            border-bottom-style:solid;
            margin-top:0px;
            margin-bottom:60px;
            border-bottom-color:#dcd9c9;
        }

        #cmsmasters_button_9oqp6tlpf {
            text-align:center;
        }

        #cmsmasters_button_9oqp6tlpf .cmsmasters_button:before {
            margin-right:.5em;
            margin-left:0;
            vertical-align:baseline;
        }

        #cmsmasters_button_9oqp6tlpf .cmsmasters_button {
            font-weight:default;
            font-style:default;
            text-transform:default;
            padding-right:38px;
            padding-left:38px;
            border-style:default;
            background-color:#2c3b4b;
            color:#ffffff;
            border-color:#2c3b4b;
        }
        #cmsmasters_button_9oqp6tlpf .cmsmasters_button:hover {
            background-color:#ff7055;
            color:#ffffff;
            border-color:#ff7055;
        }

        #cmsmasters_row_53dt8werg6 .cmsmasters_row_outer_parent {
            padding-top: 0px;
        }

        #cmsmasters_row_53dt8werg6 .cmsmasters_row_outer_parent {
            padding-bottom: 0px;
        }

        #cmsmasters_row_53dt8werg6 .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-left:0%;
        }
        #cmsmasters_row_53dt8werg6 .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-right:0%;
        }

        #cmsmasters_column_6717a6c163 {
            border-style:default;
            background-image: url(wp-content/uploads/2015/11/2.jpg);
            background-position: top center;
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-size: cover;
        }

        #cmsmasters_column_6717a6c163 .cmsmasters_column_inner {
            padding: 11% 11% 11% 11%;
        }


        #cmsmasters_divider_8phfopf04m {
            border-bottom-width:4px;
            border-bottom-style:solid;
            margin-top:0px;
            margin-bottom:13px;
            border-bottom-color:#414349;
        }

        #cmsmasters_heading_0kisdlztbb {
            text-align:left;
            margin-top:0px;
            margin-bottom:12px;
        }

        #cmsmasters_heading_0kisdlztbb .cmsmasters_heading {
            text-align:left;
        }

        #cmsmasters_heading_0kisdlztbb .cmsmasters_heading, #cmsmasters_heading_0kisdlztbb .cmsmasters_heading a {
            font-size:24px;
            line-height:32px;
            color:#ffffff;
        }

        #cmsmasters_heading_0kisdlztbb .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_0kisdlztbb .cmsmasters_heading_divider {
        }



        #cmsmasters_divider_oo0493riro {
            border-bottom-width:1px;
            border-bottom-style:solid;
            margin-top:0px;
            margin-bottom:20px;
            border-bottom-color:#414349;
        }

        #cmsmasters_heading_52h8v3j6u9 {
            text-align:left;
            margin-top:0px;
            margin-bottom:37px;
        }

        #cmsmasters_heading_52h8v3j6u9 .cmsmasters_heading {
            text-align:left;
        }

        #cmsmasters_heading_52h8v3j6u9 .cmsmasters_heading, #cmsmasters_heading_52h8v3j6u9 .cmsmasters_heading a {
            font-size:20px;
            line-height:26px;
            color:#ffffff;
        }

        #cmsmasters_heading_52h8v3j6u9 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_52h8v3j6u9 .cmsmasters_heading_divider {
        }



        #cmsmasters_button_5cdx8c501 {
            float:left;
        }

        #cmsmasters_button_5cdx8c501 .cmsmasters_button:before {
            margin-right:.5em;
            margin-left:0;
            vertical-align:baseline;
        }

        #cmsmasters_button_5cdx8c501 .cmsmasters_button {
            font-weight:default;
            font-style:default;
            text-transform:default;
            padding-right:30px;
            padding-left:30px;
            border-style:default;
            background-color:#ff7055;
            color:#ffffff;
            border-color:#ff7055;
        }
        #cmsmasters_button_5cdx8c501 .cmsmasters_button:hover {
            background-color:#ffffff;
            color:#ff7055;
            border-color:#ffffff;
        }


        #cmsmasters_column_47b1d0de2b {
            background-color:#ff7055;
            border-style:default;
        }

        #cmsmasters_column_47b1d0de2b .cmsmasters_column_inner {
            padding: 11% 11% 7.3% 11%;
        }


        #cmsmasters_divider_dn7cz6utz6 {
            border-bottom-width:4px;
            border-bottom-style:solid;
            margin-top:0px;
            margin-bottom:13px;
            border-bottom-color:#ff8d77;
        }

        #cmsmasters_heading_puox419i3s {
            text-align:left;
            margin-top:0px;
            margin-bottom:12px;
        }

        #cmsmasters_heading_puox419i3s .cmsmasters_heading {
            text-align:left;
        }

        #cmsmasters_heading_puox419i3s .cmsmasters_heading, #cmsmasters_heading_puox419i3s .cmsmasters_heading a {
            font-size:24px;
            line-height:32px;
            color:#ffffff;
        }

        #cmsmasters_heading_puox419i3s .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_puox419i3s .cmsmasters_heading_divider {
        }



        #cmsmasters_divider_rx3olw7hf {
            border-bottom-width:1px;
            border-bottom-style:solid;
            margin-top:0px;
            margin-bottom:20px;
            border-bottom-color:#ff8d77;
        }

        #cmsmasters_heading_zlrqh2exy {
            text-align:left;
            margin-top:0px;
            margin-bottom:37px;
        }

        #cmsmasters_heading_zlrqh2exy .cmsmasters_heading {
            text-align:left;
        }

        #cmsmasters_heading_zlrqh2exy .cmsmasters_heading, #cmsmasters_heading_zlrqh2exy .cmsmasters_heading a {
            font-size:20px;
            line-height:26px;
            color:#ffffff;
        }

        #cmsmasters_heading_zlrqh2exy .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_zlrqh2exy .cmsmasters_heading_divider {
        }



        #cmsmasters_column_b7f9dd2e6e {
            background-color:#2c3b4b;
            border-style:default;
        }

        #cmsmasters_column_b7f9dd2e6e .cmsmasters_column_inner {
            padding: 11% 11% 11% 11%;
        }


        #cmsmasters_divider_icbhi2sgcc {
            border-bottom-width:4px;
            border-bottom-style:solid;
            margin-top:0px;
            margin-bottom:13px;
            border-bottom-color:#56626f;
        }

        #cmsmasters_heading_xfdgrk06x {
            text-align:left;
            margin-top:0px;
            margin-bottom:12px;
        }

        #cmsmasters_heading_xfdgrk06x .cmsmasters_heading {
            text-align:left;
        }

        #cmsmasters_heading_xfdgrk06x .cmsmasters_heading, #cmsmasters_heading_xfdgrk06x .cmsmasters_heading a {
            font-size:24px;
            line-height:32px;
            color:#ffffff;
        }

        #cmsmasters_heading_xfdgrk06x .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_xfdgrk06x .cmsmasters_heading_divider {
        }



        #cmsmasters_divider_qve45x4bkd {
            border-bottom-width:1px;
            border-bottom-style:solid;
            margin-top:0px;
            margin-bottom:20px;
            border-bottom-color:#56626f;
        }

        #cmsmasters_heading_ak6xcco3x8 {
            text-align:left;
            margin-top:0px;
            margin-bottom:37px;
        }

        #cmsmasters_heading_ak6xcco3x8 .cmsmasters_heading {
            text-align:left;
        }

        #cmsmasters_heading_ak6xcco3x8 .cmsmasters_heading, #cmsmasters_heading_ak6xcco3x8 .cmsmasters_heading a {
            font-size:20px;
            line-height:26px;
            color:#ffffff;
        }

        #cmsmasters_heading_ak6xcco3x8 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_ak6xcco3x8 .cmsmasters_heading_divider {
        }



        #cmsmasters_button_wfvdd53hdl {
            float:left;
        }

        #cmsmasters_button_wfvdd53hdl .cmsmasters_button:before {
            margin-right:.5em;
            margin-left:0;
            vertical-align:baseline;
        }

        #cmsmasters_button_wfvdd53hdl .cmsmasters_button {
            font-weight:default;
            font-style:default;
            text-transform:default;
            padding-right:30px;
            padding-left:30px;
            border-style:default;
            background-color:#485567;
            color:#ffffff;
            border-color:#485567;
        }
        #cmsmasters_button_wfvdd53hdl .cmsmasters_button:hover {
            background-color:#ffffff;
            color:#485567;
            border-color:#ffffff;
        }


    </style>
    <link rel='stylesheet' id='good-food-icons-css'  href='/wp-content/themes/good-food/css/fontello.css?ver=1.0.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='good-food-icons-custom-css'  href='/wp-content/themes/good-food/theme-vars/theme-style/css/fontello-custom.css?ver=1.0.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='animate-css'  href='/wp-content/themes/good-food/css/animate.css?ver=1.0.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='ilightbox-css'  href='/wp-content/themes/good-food/css/ilightbox.css?ver=2.2.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='ilightbox-skin-dark-css'  href='/wp-content/themes/good-food/css/ilightbox-skins/dark-skin.css?ver=2.2.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='good-food-fonts-schemes-css'  href='/wp-content/uploads/cmsmasters_styles/good-food.css?ver=1.0.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='google-fonts-css'  href='//fonts.googleapis.com/css?family=Lora%3A400%2C400i%2C700%2C700i%7CKarla%3A400%2C400i%2C700%2C700i%7COswald%3A300%2C400%2C700&#038;ver=5.0.4' type='text/css' media='all' />
    <link rel='stylesheet' id='good-food-theme-vars-style-css'  href='/wp-content/themes/good-food/theme-vars/theme-style/css/vars-style.css?ver=1.0.0' type='text/css' media='screen, print' />
    <link rel='stylesheet' id='good-food-gutenberg-frontend-style-css'  href='/wp-content/themes/good-food/gutenberg/cmsmasters-framework/theme-style/css/frontend-style.css?ver=1.0.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='good-food-woocommerce-style-css'  href='/wp-content/themes/good-food/woocommerce/cmsmasters-framework/theme-style/css/plugin-style.css?ver=1.0.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='good-food-woocommerce-adaptive-css'  href='/wp-content/themes/good-food/woocommerce/cmsmasters-framework/theme-style/css/plugin-adaptive.css?ver=1.0.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='good-food-tribe-events-style-css'  href='/wp-content/themes/good-food/tribe-events/cmsmasters-framework/theme-style/css/plugin-style.css?ver=1.0.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='good-food-tribe-events-adaptive-css'  href='/wp-content/themes/good-food/tribe-events/cmsmasters-framework/theme-style/css/plugin-adaptive.css?ver=1.0.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='theme-cmsmasters-rating-style-css'  href='/wp-content/themes/good-food/cmsmasters-wp-postratings/cmsmasters-framework/theme-style/css/plugin-style.css?ver=1.0.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='tribe-events-bootstrap-datepicker-css-css'  href='/wp-content/plugins/the-events-calendar/vendor/bootstrap-datepicker/css/bootstrap-datepicker.standalone.min.css?ver=4.7.3' type='text/css' media='all' />
    <script type='text/javascript'>
        /* <![CDATA[ */
        var LS_Meta = {"v":"6.7.6"};
        /* ]]> */
    </script>
    <script type='text/javascript' src='/wp-content/plugins/LayerSlider/static/layerslider/js/greensock.js?ver=1.19.0'></script>
    <script type='text/javascript' src='/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
    <script type='text/javascript' src='/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
    <script type='text/javascript' src='/wp-content/plugins/LayerSlider/static/layerslider/js/layerslider.kreaturamedia.jquery.js?ver=6.7.6'></script>
    <script type='text/javascript' src='/wp-content/plugins/LayerSlider/static/layerslider/js/layerslider.transitions.js?ver=6.7.6'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var Cli_Data = {"nn_cookie_ids":[],"cookielist":[]};
        /* ]]> */
    </script>
    <script type='text/javascript' src='/wp-content/plugins/cookie-law-info/public/js/cookie-law-info-public.js?ver=1.7.3'></script>
    <script type='text/javascript' src='/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.8.1'></script>
    <script type='text/javascript' src='/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8.1'></script>
    <script type='text/javascript' src='/wp-content/themes/good-food/js/debounced-resize.min.js?ver=1.0.0'></script>
    <script type='text/javascript' src='/wp-content/themes/good-food/js/modernizr.min.js?ver=1.0.0'></script>
    <script type='text/javascript' src='/wp-content/themes/good-food/js/respond.min.js?ver=1.0.0'></script>
    <script type='text/javascript' src='/wp-content/themes/good-food/js/jquery.iLightBox.min.js?ver=2.2.0'></script>
    <meta name="generator" content="Powered by LayerSlider 6.7.6 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress." />
    <!-- LayerSlider updates and docs at: https://layerslider.kreaturamedia.com -->
    <link rel='https://api.w.org/' href='wp-json/' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 5.0.4" />
    <meta name="generator" content="WooCommerce 3.5.3" />
    <link rel="canonical" href="recipes/" />
    <link rel='shortlink' href='?p=11812' />
    <meta name="tec-api-version" content="v1"><meta name="tec-api-origin" content="/"><link rel="https://theeventscalendar.com/" href="wp-json/tribe/events/v1/" />	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
    <script type="text/javascript">
        var cli_flush_cache=2;
    </script>
    <meta name="generator" content="Powered by Slider Revolution 5.4.8.1 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
    <link rel="icon" href="/wp-content/uploads/2017/07/cropped-favicon-2-60x60.png" sizes="32x32" />
    <link rel="icon" href="/wp-content/uploads/2017/07/cropped-favicon-2-300x300.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="wp-content/uploads/2017/07/cropped-favicon-2-300x300.png" />
    <meta name="msapplication-TileImage" content="wp-content/uploads/2017/07/cropped-favicon-2-300x300.png" />
    <script type="text/javascript">function setREVStartSize(e){
            try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
                if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
            }catch(d){console.log("Failure at Presize of Slider:"+d)}
        };</script>
</head>
<body data-rsssl=1 class="project-template-default single single-project postid-13125 single-format-standard woocommerce-no-js tribe-no-js">

<div class="cmsmasters_header_search_form">
    <span class="cmsmasters_header_search_form_close cmsmasters_theme_icon_cancel"></span><form method="get" action="#">
        <div class="cmsmasters_header_search_form_field">
            <button type="submit" class="cmsmasters_theme_icon_search"></button>
            <input type="search" name="s" placeholder="Enter Keywords" value="" />
        </div>
    </form></div>
<!-- Start Page -->
<div id="page" class="csstransition chrome_only cmsmasters_boxed fixed_header enable_header_bottom cmsmasters_heading_after_header hfeed site">

    <!-- Start Main -->
    <div id="main">

        <!-- Start Header -->
        <header id="header">
            <div class="header_mid" data-height="65"><div class="header_mid_outer"><div class="header_mid_inner"><div class="header_mid_inner_cont">
                           <!-- <div class="social_wrap">
                                <div class="social_wrap_inner">
                                    <ul>
                                        <li>
                                            <a href="#" class="cmsmasters_social_icon cmsmasters_social_icon_1 cmsmasters-icon-linkedin cmsmasters_social_icon_color" title="Linkedin"></a>
                                        </li>
                                        <li>
                                            <a href="#" class="cmsmasters_social_icon cmsmasters_social_icon_2 cmsmasters-icon-facebook-1 cmsmasters_social_icon_color" title="Facebook" target="_blank"></a>
                                        </li>
                                        <li>
                                            <a href="#" class="cmsmasters_social_icon cmsmasters_social_icon_3 cmsmasters-icon-google cmsmasters_social_icon_color" title="Google" target="_blank"></a>
                                        </li>
                                        <li>
                                            <a href="#" class="cmsmasters_social_icon cmsmasters_social_icon_4 cmsmasters-icon-twitter cmsmasters_social_icon_color" title="Twitter" target="_blank"></a>
                                        </li>
                                        <li>
                                            <a href="#" class="cmsmasters_social_icon cmsmasters_social_icon_5 cmsmasters-icon-vimeo cmsmasters_social_icon_color" title="Vimeo" target="_blank"></a>
                                        </li>
                                    </ul>
                                </div>
                            </div> -->
                            <!--
                            <div class="mid_search_but_wrap">
                                <a href="javascript:void(0)" class="mid_search_but cmsmasters_header_search_but cmsmasters_theme_icon_search"></a>
                            </div>
                           -->
                            <div class="logo_wrap">
                                <a href="/" title="Good Food" class="logo">
                                    <img src="/wp-content/themes/good-food/theme-vars/theme-style/img/logo.png" alt="Good Food" />
                                    <img class="logo_retina" src="/wp-content/themes/good-food/theme-vars/theme-style/img/logo_retina.png" alt="Good Food" width="380" height="106" />
                                </a>
                            </div>

                            <div class="resp_mid_nav_wrap">
                                <div class="resp_mid_nav_outer">
                                    <a class="responsive_nav resp_mid_nav cmsmasters_theme_icon_resp_nav" href="javascript:void(0)"></a></div></div><!-- Start Navigation --><div class="mid_nav_wrap"><nav><div class="menu-main-menu-container">
                                        <ul id="navigation" class="mid_nav navigation">
                                            <li id="menu-item-13035" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-has-children menu-item-13035 menu-item-depth-0">
                                                <a href="/">
                                                    <span class="nav_item_wrap">
                                                        <span class="nav_title">Главная</span>
                                                    </span>
                                                </a>
                                            </li>

                                            <li id="menu-item-12979" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-12979 menu-item-depth-0"><a><span class="nav_item_wrap"><span class="nav_title">Рецепты</span></span></a>
                                                <ul class="sub-menu">
                                                    <li id="menu-item-13028" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13028 menu-item-depth-1">
                                                        <a href="/menu/goryachie-bliuda"><span class="nav_item_wrap"><span class="nav_title">Горячие блюда</span></span></a>	</li>
                                                    <li id="menu-item-13039" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13039 menu-item-depth-1">
                                                        <a href="/menu/supy"><span class="nav_item_wrap"><span class="nav_title">Супы</span></span></a>	</li>
                                                    <li id="menu-item-13037" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13037 menu-item-depth-1">
                                                        <a href="/menu/zakuski"><span class="nav_item_wrap"><span class="nav_title">Закуски</span></span></a>	</li>
                                                    <li id="menu-item-12994" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12994 menu-item-depth-1">
                                                        <a href="/menu/salaty"><span class="nav_item_wrap"><span class="nav_title">Салаты</span></span></a>	</li>
                                                    <li id="menu-item-13038" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13038 menu-item-depth-1">
                                                        <a href="/menu/sousy"><span class="nav_item_wrap"><span class="nav_title">Соусы</span></span></a>	</li>
                                                    <li id="menu-item-13038" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13038 menu-item-depth-1">
                                                        <a href="/menu/vypechka"><span class="nav_item_wrap"><span class="nav_title">Выпечка</span></span></a>	</li>
                                                    <li id="menu-item-13038" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13038 menu-item-depth-1">
                                                        <a href="/menu/deserty"><span class="nav_item_wrap"><span class="nav_title">Десерты</span></span></a>	</li>
                                                    <li id="menu-item-13038" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13038 menu-item-depth-1">
                                                        <a href="/menu/napitky"><span class="nav_item_wrap"><span class="nav_title">Напитки</span></span></a>	</li>
                                                    <li id="menu-item-13038" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13038 menu-item-depth-1">
                                                        <a href="/menu/zagotovki"><span class="nav_item_wrap"><span class="nav_title">Заготовки</span></span></a>	</li>
                                                </ul>
                                            </li>
                                        </ul></div></nav></div><!-- Finish Navigation --></div></div></div></div><div class="header_bot" data-height="210"><div class="header_bot_outer"><div class="header_bot_inner"><div class="logo_wrap"><a href="/" title="Good Food" class="logo">
                                <img src="/wp-content/themes/good-food/theme-vars/theme-style/img/logo.png" alt="Good Food" />
                                <img class="logo_retina" src="/wp-content/themes/good-food/theme-vars/theme-style/img/logo_retina.png" alt="Good Food" width="380" height="106" />
                            </a>
                        </div></div></div></div></header>
        <!-- Finish Header -->
