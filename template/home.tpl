<div class="middle_inner cmsmasters_middle_headline_disabled">
    <div class="content_wrap fullwidth">

        <!-- Start Content -->
        <div class="middle_content entry"></div></div><div id="cmsmasters_row_1nq1g9fbyt" class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
        <div class="cmsmasters_row_outer_parent">
            <div class="cmsmasters_row_outer">
                <div class="cmsmasters_row_inner">
                    <div class="cmsmasters_row_margin cmsmasters_11">
                        <div id="cmsmasters_column_g7af8pgtav" class="cmsmasters_column one_first">
                            <div class="cmsmasters_column_inner"><div id="cmsmasters_heading_9vr4xbdakl" class="cmsmasters_heading_wrap cmsmasters_heading_align_center">
                                    <h3 class="cmsmasters_heading">RECIPES</h3>
                                </div><div id="cmsmasters_divider_24al8dhwhq" class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div>
                            </div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="cmsmasters_row_nqxpnixor" class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_recipes_custom_blocks cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
        <div class="cmsmasters_row_outer_parent">
            <div class="cmsmasters_row_outer">
                <div class="cmsmasters_row_inner cmsmasters_row_no_margin">
                    <div class="cmsmasters_row_margin cmsmasters_row_columns_behavior cmsmasters_141214">
                        <div id="cmsmasters_column_mcvqxars8s" class="cmsmasters_column one_fourth">
                            <div class="cmsmasters_column_inner"><div id="cmsmasters_fb_5ljq5091" class="cmsmasters_featured_block">
                                    <div class="featured_block_inner">
                                        <div class="featured_block_text"><div id="cmsmasters_heading_sr958mosp" class="cmsmasters_heading_wrap cmsmasters_heading_align_center">
                                                <h3 class="cmsmasters_heading">Найдено</h3>
                                            </div><div id="cmsmasters_heading_qswstud0sa" class="cmsmasters_heading_wrap cmsmasters_heading_align_center">
                                                <h2 class="cmsmasters_heading">24,400+ тецептов</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div></div>
                        <div id="cmsmasters_column_z0jym1qjff" class="cmsmasters_column one_half">
                            <div class="cmsmasters_column_inner"><div id="cmsmasters_fb_4iysws4bdh" class="cmsmasters_featured_block">
                                    <div class="featured_block_inner">
                                        <div class="featured_block_text">
                                            <div style="text-align: center;"><span style="color: #2c3b4b; font-size: 18px; line-height: 32px;">Whether you’re looking for healthy recipes or ideas to use up last night’s chicken, we’ve more than 5,500+ tested recipes to choose from, so you’re sure to find the perfect dish. Take a look at our most popular recipes.</span></div>

                                        </div>
                                    </div>
                                </div>
                            </div></div>
                        <div id="cmsmasters_column_uchnjyjmnr" class="cmsmasters_column one_fourth">
                            <div class="cmsmasters_column_inner"><div id="cmsmasters_fb_0x2n85yne" class="cmsmasters_featured_block">
                                    <div class="featured_block_inner">
                                        <div class="featured_block_text">
                                            <div><div class="cmsmasters_sidebar sidebar_layout_11"><aside id="search-4" class="widget widget_search"><div class="search_bar_wrap">
                                                            <form method="get" action="/">
                                                                <p class="search_field">
                                                                    <input name="s" placeholder="Search..." value="" type="search">
                                                                </p>
                                                                <p class="search_button">
                                                                    <button type="submit" class="cmsmasters_theme_icon_search"></button>
                                                                </p>
                                                            </form>
                                                        </div></aside><div class="cl"></div>
                                                </div><div id="cmsmasters_heading_cfo3boswfi" class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                                    <h5 class="cmsmasters_heading">For example: Roast Turkey </h5>
                                                </div></div>

                                        </div>
                                    </div>
                                </div>
                            </div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="cmsmasters_row_x61kquuw89" class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
        <div class="cmsmasters_row_outer_parent">
            <div class="cmsmasters_row_outer">
                <div class="cmsmasters_row_inner">
                    <div class="cmsmasters_row_margin cmsmasters_11">
                        <div id="cmsmasters_column_4yqgz49ri" class="cmsmasters_column one_first">
                            <div class="cmsmasters_column_inner"><div id="cmsmasters_heading_50200ddntl" class="cmsmasters_heading_wrap cmsmasters_heading_align_center cmsmasters_heading_shaped">
                                    <h6 class="cmsmasters_heading">Рецепты</h6>
                                </div><div class="cmsmasters_tabs tabs_mode_tab">
                                    <ul class="cmsmasters_tabs_list">
                                        <li id="cmsmasters_tabs_list_item_7ayu4pm4m" class="cmsmasters_tabs_list_item current_tab">
                                            <a href="#">
                                                <span>Завтраки</span>
                                            </a>
                                        </li><li id="cmsmasters_tabs_list_item_qd84warae" class="cmsmasters_tabs_list_item">
                                            <a href="#">
                                                <span>Обеды</span>
                                            </a>
                                        </li><li id="cmsmasters_tabs_list_item_io5kbqcpt" class="cmsmasters_tabs_list_item">
                                            <a href="#">
                                                <span>Закуски</span>
                                            </a>
                                        </li><li id="cmsmasters_tabs_list_item_t3aeett72" class="cmsmasters_tabs_list_item">
                                            <a href="#">
                                                <span>Дессерты</span>
                                            </a>
                                        </li><li id="cmsmasters_tabs_list_item_y2xa6r1i3h" class="cmsmasters_tabs_list_item">
                                            <a href="#">
                                                <span>Котейли</span>
                                            </a>
                                        </li><li id="cmsmasters_tabs_list_item_itr042h72n" class="cmsmasters_tabs_list_item">
                                            <a href="#">
                                                <span>Выпечка</span>
                                            </a>
                                        </li></ul>
                                    <div class="cmsmasters_tabs_wrap">
                                        <div id="cmsmasters_tab_7ayu4pm4m" class="cmsmasters_tab active_tab">
                                            <div class="cmsmasters_tab_inner">
                                                <div id="portfolio_ye2asggeqf" class="cmsmasters_wrap_portfolio entry-summary" data-layout="grid" data-layout-mode="perfect" data-url="/wp-content/plugins/cmsmasters-content-composer/" data-orderby="date" data-order="ASC" data-count="4" data-categories="breakfast" data-metadata="title,categories,rollover"><div class="portfolio grid large_gap perfect cmsmasters_4 isotope" style="position: relative; height: 1012.28px;"><!-- Start Project Grid Article -->
                                                        <article id="post-13135" class="cmsmasters_project_grid post-13135 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-breakfast shortcode_animated" data-category="breakfast" style="width: 485px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 30 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 45 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Difficult</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/ddzyotzunbk-eaters-collective.jpg" class="full-width wp-post-image" alt="Creamy Mushroom Pasta" title="Creamy Mushroom Pasta" srcset="/wp-content/uploads/2017/06/ddzyotzunbk-eaters-collective.jpg 810w, /wp-content/uploads/2017/06/ddzyotzunbk-eaters-collective-300x233.jpg 300w, /wp-content/uploads/2017/06/ddzyotzunbk-eaters-collective-768x597.jpg 768w, /wp-content/uploads/2017/06/ddzyotzunbk-eaters-collective-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/creamy-mushroom-pasta/" title="Creamy Mushroom Pasta" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/malenkii-ovoshnoi-omlet">МАЛЕНЬКИЙ ОВОЩНОЙ ОМЛЕТ   </a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">50 мин</span><span class="cmsmasters_project_category"><a href="" class="cmsmasters_cat_color cmsmasters_cat_39" rel="category tag">Завтрак</a></span></footer></div><span class="dn meta-date">20170628134642</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13142" class="cmsmasters_project_grid post-13142 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-breakfast shortcode_animated" data-category="breakfast" style="width: 485px; position: absolute; left: 485px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 10 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 25 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Difficult</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/joseph-gonzalez-80541.jpg" class="full-width wp-post-image" alt="Great Easy Waffles" title="Great Easy Waffles" srcset="/wp-content/uploads/2017/06/joseph-gonzalez-80541.jpg 810w, /wp-content/uploads/2017/06/joseph-gonzalez-80541-300x233.jpg 300w, /wp-content/uploads/2017/06/joseph-gonzalez-80541-768x597.jpg 768w, /wp-content/uploads/2017/06/joseph-gonzalez-80541-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/great-easy-waffles/" title="Great Easy Waffles" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/limonno-chernichnye-oladi">Лимонно-черничные оладьи</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">20 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/breakfast/" class="cmsmasters_cat_color cmsmasters_cat_39" rel="category tag">Завтрак</a></span></footer></div><span class="dn meta-date">20170628135256</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13145" class="cmsmasters_project_grid post-13145 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-breakfast shortcode_animated" data-category="breakfast" style="width: 485px; position: absolute; left: 0px; top: 506.141px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 10 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 25 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Average</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/ritqq_qloic-ashwin-vaswani.jpg" class="full-width wp-post-image" alt="Great Easy Waffles" title="Great Easy Waffles" srcset="/wp-content/uploads/2017/06/ritqq_qloic-ashwin-vaswani.jpg 810w, /wp-content/uploads/2017/06/ritqq_qloic-ashwin-vaswani-300x233.jpg 300w, /wp-content/uploads/2017/06/ritqq_qloic-ashwin-vaswani-768x597.jpg 768w, /wp-content/uploads/2017/06/ritqq_qloic-ashwin-vaswani-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/great-easy-waffles-2/" title="Great Easy Waffles" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/francuzskie-maffiny">Французские маффины</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">55 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/breakfast/" class="cmsmasters_cat_color cmsmasters_cat_39" rel="category tag">Завтрак</a></span></footer></div><span class="dn meta-date">20170628135647</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13148" class="cmsmasters_project_grid post-13148 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-breakfast shortcode_animated" data-category="breakfast" style="width: 485px; position: absolute; left: 485px; top: 506.141px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 5 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 20 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Average</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/toa-heftiba-88630.jpg" class="full-width wp-post-image" alt="Желе из свежих киви с ревенем" title="Желе из свежих киви с ревенем" srcset="/wp-content/uploads/2017/06/toa-heftiba-88630.jpg 810w, /wp-content/uploads/2017/06/toa-heftiba-88630-300x233.jpg 300w, /wp-content/uploads/2017/06/toa-heftiba-88630-768x597.jpg 768w, /wp-content/uploads/2017/06/toa-heftiba-88630-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/jele-iz-svejih-kivi-s-revenem" title="Желе из свежих киви с ревенем" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/good-old-pancakes/">Желе из свежих киви с ревенем</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">40 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/breakfast/" class="cmsmasters_cat_color cmsmasters_cat_39" rel="category tag">Завтрак</a></span></footer></div><span class="dn meta-date">20170628140501</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                    </div></div>
                                            </div>
                                        </div><div id="cmsmasters_tab_qd84warae" class="cmsmasters_tab">
                                            <div class="cmsmasters_tab_inner">
                                                <div id="portfolio_7feg0k7ge" class="cmsmasters_wrap_portfolio entry-summary" data-layout="grid" data-layout-mode="perfect" data-url="/wp-content/plugins/cmsmasters-content-composer/" data-orderby="date" data-order="ASC" data-count="4" data-categories="dinner" data-metadata="title,categories,rollover"><div class="portfolio grid large_gap perfect cmsmasters_4 isotope" style="position: relative; height: 0px;"><!-- Start Project Grid Article -->
                                                        <article id="post-13169" class="cmsmasters_project_grid post-13169 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-dinner shortcode_animated" data-category="dinner" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 10 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 40 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 50 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Average</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/75636117.jpg" class="full-width wp-post-image" alt="Бефстроганов с грибами и луком" title="Бефстроганов с грибами и луком" srcset="/wp-content/uploads/2017/06/75636117.jpg 810w, /wp-content/uploads/2017/06/75636117-300x233.jpg 300w, /wp-content/uploads/2017/06/75636117-768x597.jpg 768w, /wp-content/uploads/2017/06/75636117-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/brontosaurus-burgers/" title="Бефстроганов с грибами и луком" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/befstroganov-s-gribami-i-lukom/">Бефстроганов с грибами и луком</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">25 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/dinner/" class="cmsmasters_cat_color cmsmasters_cat_41" rel="category tag">Обед</a></span></footer></div><span class="dn meta-date">20170629124203</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13176" class="cmsmasters_project_grid post-13176 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-dinner shortcode_animated" data-category="dinner" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 20 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 1 h</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 1 h 20 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Difficult</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/111384038.jpg" class="full-width wp-post-image" alt=" Мясной рулет и макароны с сыром" title=" Мясной рулет и макароны с сыром" srcset="/wp-content/uploads/2017/06/111384038.jpg 810w, /wp-content/uploads/2017/06/111384038-300x233.jpg 300w, /wp-content/uploads/2017/06/111384038-768x597.jpg 768w, /wp-content/uploads/2017/06/111384038-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/moroccan-shabbat-fish/" title=" Мясной рулет и макароны с сыром" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/myasnoi-rulet-i-makarony-s-syrom">Мясной рулет и макароны с сыром</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">1 ч</span><span class="cmsmasters_project_category"><a href="/pj-categs/dinner/" class="cmsmasters_cat_color cmsmasters_cat_41" rel="category tag">Dinner</a></span></footer></div><span class="dn meta-date">20170629125410</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13182" class="cmsmasters_project_grid post-13182 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-dinner shortcode_animated" data-category="dinner" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 1 h</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 2 h 15 m</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Difficult</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/dqddd3iwt2i-neha-deshmukh.jpg" class="full-width wp-post-image" alt="Рагу из говядины" title="Рагу из говядины" srcset="/wp-content/uploads/2017/06/dqddd3iwt2i-neha-deshmukh.jpg 810w, /wp-content/uploads/2017/06/dqddd3iwt2i-neha-deshmukh-300x233.jpg 300w, /wp-content/uploads/2017/06/dqddd3iwt2i-neha-deshmukh-768x597.jpg 768w, /wp-content/uploads/2017/06/dqddd3iwt2i-neha-deshmukh-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/bean-salad-with-celery/" title="Рагу из говядины" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/ragu-iz-govyadiny">Рагу из говядины</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">1 ч</span><span class="cmsmasters_project_category"><a href="/pj-categs/dinner/" class="cmsmasters_cat_color cmsmasters_cat_41" rel="category tag">Dinner</a></span></footer></div><span class="dn meta-date">20170629130416</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13185" class="cmsmasters_project_grid post-13185 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-dinner shortcode_animated" data-category="dinner" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 10 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 10 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 20 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Difficult</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/qviri4ueqzy-adrien-sala.jpg" class="full-width wp-post-image" alt="Мясной рулет с грибами" title="Мясной рулет с грибами" srcset="/wp-content/uploads/2017/06/qviri4ueqzy-adrien-sala.jpg 810w, /wp-content/uploads/2017/06/qviri4ueqzy-adrien-sala-300x233.jpg 300w, /wp-content/uploads/2017/06/qviri4ueqzy-adrien-sala-768x597.jpg 768w, /wp-content/uploads/2017/06/qviri4ueqzy-adrien-sala-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/appetizer-mussels/" title="Мясной рулет с грибами" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/myasnoi-rulet-s-gribami/">Мясной рулет с грибами</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">1 ч</span><span class="cmsmasters_project_category"><a href="/pj-categs/dinner/" class="cmsmasters_cat_color cmsmasters_cat_41" rel="category tag">Dinner</a></span></footer></div><span class="dn meta-date">20170629132147</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                    </div></div>
                                            </div>
                                        </div><div id="cmsmasters_tab_io5kbqcpt" class="cmsmasters_tab">
                                            <div class="cmsmasters_tab_inner">
                                                <div id="portfolio_jcivjq44bk" class="cmsmasters_wrap_portfolio entry-summary" data-layout="grid" data-layout-mode="perfect" data-url="/wp-content/plugins/cmsmasters-content-composer/" data-orderby="date" data-order="ASC" data-count="4" data-categories="snacks" data-metadata="title,categories,rollover"><div class="portfolio grid large_gap perfect cmsmasters_4 isotope" style="position: relative; height: 0px;"><!-- Start Project Grid Article -->
                                                        <article id="post-13189" class="cmsmasters_project_grid post-13189 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-snacks shortcode_animated" data-category="snacks" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 5 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 10 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Beginner</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/112187194.jpg" class="full-width wp-post-image" alt="Сэндвич со свининой" title="Сэндвич со свининой" srcset="/wp-content/uploads/2017/06/112187194.jpg 810w, /wp-content/uploads/2017/06/112187194-300x233.jpg 300w, /wp-content/uploads/2017/06/112187194-768x597.jpg 768w, /wp-content/uploads/2017/06/112187194-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/turkey-sandwich/" title="Сэндвич со свининой" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/sendvich-so-svininoi">Сэндвич со свининой</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">3 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/snacks/" class="cmsmasters_cat_color cmsmasters_cat_42" rel="category tag">Snacks</a></span></footer></div><span class="dn meta-date">20170629133058</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13196" class="cmsmasters_project_grid post-13196 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-snacks shortcode_animated" data-category="snacks" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 10 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 30 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 40 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Average</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/azoqcek2kuq-herson-rodriguez.jpg" class="full-width wp-post-image" alt="Французские тосты" title="Французские тосты" srcset="/wp-content/uploads/2017/06/azoqcek2kuq-herson-rodriguez.jpg 810w, /wp-content/uploads/2017/06/azoqcek2kuq-herson-rodriguez-300x233.jpg 300w, /wp-content/uploads/2017/06/azoqcek2kuq-herson-rodriguez-768x597.jpg 768w, /wp-content/uploads/2017/06/azoqcek2kuq-herson-rodriguez-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/beer-cheese-dip/" title="Французские тосты" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/francuzskie-tosty">Французские тосты</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">20 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/snacks/" class="cmsmasters_cat_color cmsmasters_cat_42" rel="category tag">Snacks</a></span></footer></div><span class="dn meta-date">20170629134112</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13199" class="cmsmasters_project_grid post-13199 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-snacks shortcode_animated" data-category="snacks" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 30 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 45 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Average </div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/e34hxlihyg8-matt-alaniz.jpg" class="full-width wp-post-image" alt="Куриный сэндвич" title="Куриный сэндвич" srcset="/wp-content/uploads/2017/06/e34hxlihyg8-matt-alaniz.jpg 810w, /wp-content/uploads/2017/06/e34hxlihyg8-matt-alaniz-300x233.jpg 300w, /wp-content/uploads/2017/06/e34hxlihyg8-matt-alaniz-768x597.jpg 768w, /wp-content/uploads/2017/06/e34hxlihyg8-matt-alaniz-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/reuben-sandwich/" title="Куриный сэндвич" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/kurinyi-sendvich">Куриный сэндвич</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">20 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/snacks/" class="cmsmasters_cat_color cmsmasters_cat_42" rel="category tag">Snacks</a></span></footer></div><span class="dn meta-date">20170629134700</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13202" class="cmsmasters_project_grid post-13202 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-snacks shortcode_animated" data-category="snacks" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 5 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 10 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Beginner</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/StockSnap_K5T076FWTJ.jpg" class="full-width wp-post-image" alt="Канапе из лука и икры" title="Канапе из лука и икры" srcset="/wp-content/uploads/2017/06/StockSnap_K5T076FWTJ.jpg 810w, /wp-content/uploads/2017/06/StockSnap_K5T076FWTJ-300x233.jpg 300w, /wp-content/uploads/2017/06/StockSnap_K5T076FWTJ-768x597.jpg 768w, /wp-content/uploads/2017/06/StockSnap_K5T076FWTJ-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/kanape-iz-luka-i-ikry" title="Канапе из лука и икры" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/kanape-iz-luka-i-ikry">Канапе из лука и икры</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">15 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/snacks/" class="cmsmasters_cat_color cmsmasters_cat_42" rel="category tag">Snacks</a></span></footer></div><span class="dn meta-date">20170629135032</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                    </div></div>
                                            </div>
                                        </div><div id="cmsmasters_tab_t3aeett72" class="cmsmasters_tab">
                                            <div class="cmsmasters_tab_inner">
                                                <div id="portfolio_trnkto34of" class="cmsmasters_wrap_portfolio entry-summary" data-layout="grid" data-layout-mode="perfect" data-url="/wp-content/plugins/cmsmasters-content-composer/" data-orderby="date" data-order="ASC" data-count="4" data-categories="dessert" data-metadata="title,categories,rollover"><div class="portfolio grid large_gap perfect cmsmasters_4 isotope" style="position: relative; height: 0px;"><!-- Start Project Grid Article -->
                                                        <article id="post-13151" class="cmsmasters_project_grid post-13151 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-dessert shortcode_animated" data-category="dessert" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 40 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Difficult</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/cream-puffs-delicious-france-confectionery-food-52539.jpg" class="full-width wp-post-image" alt="Виноградное желе" title="Виноградное желе" srcset="/wp-content/uploads/2017/06/cream-puffs-delicious-france-confectionery-food-52539.jpg 810w, /wp-content/uploads/2017/06/cream-puffs-delicious-france-confectionery-food-52539-300x233.jpg 300w, /wp-content/uploads/2017/06/cream-puffs-delicious-france-confectionery-food-52539-768x597.jpg 768w, /wp-content/uploads/2017/06/cream-puffs-delicious-france-confectionery-food-52539-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/vinogradnoe-jele" title="Виноградное желе" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/vinogradnoe-jele">Виноградное желе</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">30 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/dessert/" class="cmsmasters_cat_color cmsmasters_cat_40" rel="category tag">Dessert</a></span></footer></div><span class="dn meta-date">20170628142146</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13158" class="cmsmasters_project_grid post-13158 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-dessert shortcode_animated" data-category="dessert" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 30 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 10 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 2 h 10 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Difficult</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/karlis-dambrans-117428.jpg" class="full-width wp-post-image" alt="Шоколадный пудинг" title="Шоколадный пудинг" srcset="/wp-content/uploads/2017/06/karlis-dambrans-117428.jpg 810w, /wp-content/uploads/2017/06/karlis-dambrans-117428-300x233.jpg 300w, /wp-content/uploads/2017/06/karlis-dambrans-117428-768x597.jpg 768w, /wp-content/uploads/2017/06/karlis-dambrans-117428-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/shokoladnyi-puding-" title="Шоколадный пудинг" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/shokoladnyi-puding-">Шоколадный пудинг</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">30 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/dessert/" class="cmsmasters_cat_color cmsmasters_cat_40" rel="category tag">Dessert</a></span></footer></div><span class="dn meta-date">20170628142928</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13161" class="cmsmasters_project_grid post-13161 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-dessert shortcode_animated" data-category="dessert" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 5 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 8 h 20 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Average</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/mantra-media-58091.jpg" class="full-width wp-post-image" alt="Чизкейк творожный" title="Чизкейк творожный" srcset="/wp-content/uploads/2017/06/mantra-media-58091.jpg 810w, /wp-content/uploads/2017/06/mantra-media-58091-300x233.jpg 300w, /wp-content/uploads/2017/06/mantra-media-58091-768x597.jpg 768w, /wp-content/uploads/2017/06/mantra-media-58091-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/chizkeik-tvorojnyi" title="Чизкейк творожный" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/chizkeik-tvorojnyi">Чизкейк творожный</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">1 ч</span><span class="cmsmasters_project_category"><a href="/pj-categs/dessert/" class="cmsmasters_cat_color cmsmasters_cat_40" rel="category tag">Dessert</a></span></footer></div><span class="dn meta-date">20170628144055</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13164" class="cmsmasters_project_grid post-13164 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-dessert shortcode_animated" data-category="dessert" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 30 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 1 h</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 2 h</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Difficult</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/pablo-merchan-montes-73076.jpg" class="full-width wp-post-image" alt="Кексы с изюмом" title="Кексы с изюмом" srcset="/wp-content/uploads/2017/06/pablo-merchan-montes-73076.jpg 810w, /wp-content/uploads/2017/06/pablo-merchan-montes-73076-300x233.jpg 300w, /wp-content/uploads/2017/06/pablo-merchan-montes-73076-768x597.jpg 768w, /wp-content/uploads/2017/06/pablo-merchan-montes-73076-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/keksy-s-iziumom" title="Кексы с изюмом" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/keksy-s-iziumom">Кексы с изюмом</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">1 ч</span><span class="cmsmasters_project_category"><a href="/pj-categs/dessert/" class="cmsmasters_cat_color cmsmasters_cat_40" rel="category tag">Dessert</a></span></footer></div><span class="dn meta-date">20170629122647</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                    </div></div>
                                            </div>
                                        </div><div id="cmsmasters_tab_y2xa6r1i3h" class="cmsmasters_tab">
                                            <div class="cmsmasters_tab_inner">
                                                <div id="portfolio_xs3tt7r9cf" class="cmsmasters_wrap_portfolio entry-summary" data-layout="grid" data-layout-mode="perfect" data-url="/wp-content/plugins/cmsmasters-content-composer/" data-orderby="date" data-order="ASC" data-count="4" data-categories="%d1%81ocktails" data-metadata="title,categories,rollover"><div class="portfolio grid large_gap perfect cmsmasters_4 isotope" style="position: relative; height: 0px;"><!-- Start Project Grid Article -->
                                                        <article id="post-13206" class="cmsmasters_project_grid post-13206 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-ocktails shortcode_animated" data-category="%d1%81ocktails" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 5 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Beginner</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/196065.jpg" class="full-width wp-post-image" alt="Сливочный коктейль с ежевикой" title="Сливочный коктейль с ежевикой" srcset="/wp-content/uploads/2017/06/196065.jpg 810w, /wp-content/uploads/2017/06/196065-300x233.jpg 300w, /wp-content/uploads/2017/06/196065-768x597.jpg 768w, /wp-content/uploads/2017/06/196065-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/slivochnyi-kokteil-s-ejevikoi" title="Сливочный коктейль с ежевикой" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/slivochnyi-kokteil-s-ejevikoi">Сливочный коктейль с ежевикой</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">8 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/%d1%81ocktails/" class="cmsmasters_cat_color cmsmasters_cat_43" rel="category tag">Сocktails</a></span></footer></div><span class="dn meta-date">20170629135958</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13213" class="cmsmasters_project_grid post-13213 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-ocktails shortcode_animated" data-category="%d1%81ocktails" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 5 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 5 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 5 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Difficult</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/55576317.jpg" class="full-width wp-post-image" alt="Коктейль с клубникой" title="Коктейль с клубникой" srcset="/wp-content/uploads/2017/06/55576317.jpg 810w, /wp-content/uploads/2017/06/55576317-300x233.jpg 300w, /wp-content/uploads/2017/06/55576317-768x597.jpg 768w, /wp-content/uploads/2017/06/55576317-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/kokteil-s-klubnikoi" title="Коктейль с клубникой" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/kokteil-s-klubnikoi">Коктейль с клубникой</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">15 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/%d1%81ocktails/" class="cmsmasters_cat_color cmsmasters_cat_43" rel="category tag">Сocktails</a></span></footer></div><span class="dn meta-date">20170629140448</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13216" class="cmsmasters_project_grid post-13216 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-ocktails shortcode_animated" data-category="%d1%81ocktails" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 10 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 10 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 10 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Beginner</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/4178415E1A.jpg" class="full-width wp-post-image" alt="Вишнёвый коктейль" title="Вишнёвый коктейль" srcset="/wp-content/uploads/2017/06/4178415E1A.jpg 810w, /wp-content/uploads/2017/06/4178415E1A-300x233.jpg 300w, /wp-content/uploads/2017/06/4178415E1A-768x597.jpg 768w, /wp-content/uploads/2017/06/4178415E1A-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/vishnyovyi-kokteil" title="Вишнёвый коктейль" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/vishnyovyi-kokteil">Вишнёвый коктейль</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">15 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/%d1%81ocktails/" class="cmsmasters_cat_color cmsmasters_cat_43" rel="category tag">Сocktails</a></span></footer></div><span class="dn meta-date">20170629140758</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13219" class="cmsmasters_project_grid post-13219 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-ocktails shortcode_animated" data-category="%d1%81ocktails" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 15 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Beginner</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2017/06/HPRJU3RU93.jpg" class="full-width wp-post-image" alt="Коктейль из лайма и киви" title="Коктейль из лайма и киви" srcset="/wp-content/uploads/2017/06/HPRJU3RU93.jpg 810w, /wp-content/uploads/2017/06/HPRJU3RU93-300x233.jpg 300w, /wp-content/uploads/2017/06/HPRJU3RU93-768x597.jpg 768w, /wp-content/uploads/2017/06/HPRJU3RU93-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/kokteil-iz-laima-i-kivi" title="Коктейль из лайма и киви" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/kokteil-iz-laima-i-kivi">Коктейль из лайма и киви</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">10 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/%d1%81ocktails/" class="cmsmasters_cat_color cmsmasters_cat_43" rel="category tag">Сocktails</a></span></footer></div><span class="dn meta-date">20170629141136</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                    </div></div>
                                            </div>
                                        </div><div id="cmsmasters_tab_itr042h72n" class="cmsmasters_tab">
                                            <div class="cmsmasters_tab_inner">
                                                <div id="portfolio_tqodcs08m6" class="cmsmasters_wrap_portfolio entry-summary" data-layout="grid" data-layout-mode="perfect" data-url="/wp-content/plugins/cmsmasters-content-composer/" data-orderby="date" data-order="DESC" data-count="4" data-categories="baking" data-metadata="title,categories,rollover"><div class="portfolio grid large_gap perfect cmsmasters_4 isotope" style="position: relative; height: 0px;"><!-- Start Project Grid Article -->
                                                        <article id="post-13125" class="cmsmasters_project_grid post-13125 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-baking shortcode_animated" data-category="baking" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 30 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 45 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 3 h</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Difficult</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2015/01/project.jpg" class="full-width wp-post-image" alt="Булочки с беконом и сыром" title="Булочки с беконом и сыром" srcset="/wp-content/uploads/2015/01/project.jpg 810w, /wp-content/uploads/2015/01/project-300x233.jpg 300w, /wp-content/uploads/2015/01/project-768x597.jpg 768w, /wp-content/uploads/2015/01/project-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/bulochki-s-bekonom-i-syrom-" title="Булочки с беконом и сыром" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/bulochki-s-bekonom-i-syrom-">Булочки с беконом и сыром</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">30 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/baking/" class="cmsmasters_cat_color cmsmasters_cat_38" rel="category tag">Baking</a></span></footer></div><span class="dn meta-date">20170628133251</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13122" class="cmsmasters_project_grid post-13122 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-baking shortcode_animated" data-category="baking" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 20 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 20 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 1 h 30 m</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Difficult</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2015/01/mario-calvo-5371.jpg" class="full-width wp-post-image" alt="Домашний хлеб с томатами" title="Домашний хлеб с томатами" srcset="/wp-content/uploads/2015/01/mario-calvo-5371.jpg 810w, /wp-content/uploads/2015/01/mario-calvo-5371-300x233.jpg 300w, /wp-content/uploads/2015/01/mario-calvo-5371-768x597.jpg 768w, /wp-content/uploads/2015/01/mario-calvo-5371-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/domashnii-hleb-s-tomatami" title="Домашний хлеб с томатами" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/domashnii-hleb-s-tomatami">Домашний хлеб с томатами</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">2 ч 30 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/baking/" class="cmsmasters_cat_color cmsmasters_cat_38" rel="category tag">Baking</a></span></footer></div><span class="dn meta-date">20170628132115</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-13119" class="cmsmasters_project_grid post-13119 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-baking shortcode_animated" data-category="baking" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Prep: 20 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook: 12 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Ready In: 56 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Beginner</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2015/01/dzkixr9fycm-jennifer-pallian-copy.jpg" class="full-width wp-post-image" alt="Американское Овсяное Печенье" title="Американское Овсяное Печенье" srcset="/wp-content/uploads/2015/01/dzkixr9fycm-jennifer-pallian-copy.jpg 810w, /wp-content/uploads/2015/01/dzkixr9fycm-jennifer-pallian-copy-300x233.jpg 300w, /wp-content/uploads/2015/01/dzkixr9fycm-jennifer-pallian-copy-768x597.jpg 768w, /wp-content/uploads/2015/01/dzkixr9fycm-jennifer-pallian-copy-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/amerikanskoe-ovsyanoe-pechene" title="Американское Овсяное Печенье" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/amerikanskoe-ovsyanoe-pechene">Американское Овсяное Печенье</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">1 ч</span><span class="cmsmasters_project_category"><a href="/pj-categs/baking/" class="cmsmasters_cat_color cmsmasters_cat_38" rel="category tag">Baking</a></span></footer></div><span class="dn meta-date">20170628131456</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                        <!-- Start Project Grid Article -->
                                                        <article id="post-9410" class="cmsmasters_project_grid post-9410 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-baking shortcode_animated" data-category="baking" style="width: 0px; position: absolute; left: 0px; top: 0px;">
                                                            <div class="project_outer">
                                                                <div class="project_outer_image_wrap"><div class="project_outer_image_wrap_cont"><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Yield: 12</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Servings: 15</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Cook Time: 60 min</div></div><div class="cmsmasters_project_info_item"><div class="cmsmasters_project_info_item_inner">Level : Difficult</div></div></div><figure class="cmsmasters_img_rollover_wrap preloader"><img width="810" height="630" src="/wp-content/uploads/2015/01/brina-blum-114390.jpg" class="full-width wp-post-image" alt="Маффины с малиновым кремом" title="Маффины с малиновым кремом" srcset="/wp-content/uploads/2015/01/brina-blum-114390.jpg 810w, /wp-content/uploads/2015/01/brina-blum-114390-300x233.jpg 300w, /wp-content/uploads/2015/01/brina-blum-114390-768x597.jpg 768w, /wp-content/uploads/2015/01/brina-blum-114390-600x467.jpg 600w" sizes="(max-width: 810px) 100vw, 810px"><div class="cmsmasters_img_rollover"><a href="/recipe/maffiny-s-malinovym-kremom" title="Маффины с малиновым кремом" class="cmsmasters_open_link"></a></div></figure></div><div class="project_inner"><header class="cmsmasters_project_header entry-header"><h4 class="cmsmasters_project_title entry-title"><a href="/recipe/maffiny-s-malinovym-kremom">Маффины с малиновым кремом</a></h4></header><footer class="cmsmasters_project_footer entry-meta"><span class="cmsmasters_project_footer_info cmsmasters-icon-time">45 мин</span><span class="cmsmasters_project_category"><a href="/pj-categs/baking/" class="cmsmasters_cat_color cmsmasters_cat_38" rel="category tag">Baking</a></span></footer></div><span class="dn meta-date">20150110132206</span>	</div>
                                                        </article>
                                                        <!-- Finish Project Grid Article -->

                                                    </div></div>
                                            </div>
                                        </div></div>
                                </div><div id="cmsmasters_divider_ofa4n8uv5f" class="cl"></div><div id="cmsmasters_button_4ls0p85qpf" class="button_wrap"><a href="/" class="cmsmasters_button"><span>all recipes</span></a></div>
                            </div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="cmsmasters_row_j492462pce" class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_recipes_custom_fb cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
        <div class="cmsmasters_row_outer_parent">
            <div class="cmsmasters_row_outer">
                <div class="cmsmasters_row_inner">
                    <div class="cmsmasters_row_margin cmsmasters_row_columns_behavior cmsmasters_1212">
                        <div id="cmsmasters_column_0d0f4a91bc" class="cmsmasters_column one_half">
                            <div class="cmsmasters_column_inner"><div id="cmsmasters_divider_ryo5vvcgl7" class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div><div id="cmsmasters_heading_u5lta4u2ga" class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                    <h3 class="cmsmasters_heading">download Good food app</h3>
                                </div><div id="cmsmasters_divider_zq86vkoas" class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div><div id="cmsmasters_heading_omgndniakg" class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                    <h2 class="cmsmasters_heading">Take out a free 30-day trial to see for yourself, or browse our unlocked recipes.</h2>
                                </div><div id="cmsmasters_button_i8rmwo0le" class="button_wrap"><a href="/contacts/" class="cmsmasters_button"><span>Download</span></a></div>
                            </div></div>
                        <div id="cmsmasters_column_af739082c0" class="cmsmasters_column one_half">
                            <div class="cmsmasters_column_inner"><div id="cmsmasters_divider_lxb01044n" class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div><div id="cmsmasters_heading_1qbiouue25" class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                    <h3 class="cmsmasters_heading">Cooking Classes from Good Food</h3>
                                </div><div id="cmsmasters_divider_wjmz2hmgr" class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div><div id="cmsmasters_heading_gu1d9fs7o" class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                    <h2 class="cmsmasters_heading">Learn to cook from award-winning chefs like Connor Walker and Max Taylor.</h2>
                                </div><div id="cmsmasters_button_2jx8m2rwr" class="button_wrap"><a href="/events/list/" class="cmsmasters_button"><span>learn more</span></a></div>
                            </div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="cmsmasters_row_buyd8jdis" class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
        <div class="cmsmasters_row_outer_parent">
            <div class="cmsmasters_row_outer">
                <div class="cmsmasters_row_inner">
                    <div class="cmsmasters_row_margin cmsmasters_11">
                        <div id="cmsmasters_column_4ktfd8idu4" class="cmsmasters_column one_first">
                            <div class="cmsmasters_column_inner"><div id="cmsmasters_heading_pov49ywg7j" class="cmsmasters_heading_wrap cmsmasters_heading_align_center">
                                    <h2 class="cmsmasters_heading">BECOME A MEMBER</h2>
                                </div><div id="cmsmasters_heading_hgxaf20me" class="cmsmasters_heading_wrap cmsmasters_heading_align_center cmsmasters_heading_shaped">
                                    <h6 class="cmsmasters_heading">Subscribe</h6>
                                </div>
                            </div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="cmsmasters_row_rhx1xftan" class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_recipes_and_pricing_custom cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
        <div class="cmsmasters_row_outer_parent">
            <div class="cmsmasters_row_outer">
                <div class="cmsmasters_row_inner">
                    <div class="cmsmasters_row_margin cmsmasters_14141414">
                        <div id="cmsmasters_column_06spw7z62" class="cmsmasters_column one_fourth">
                            <div class="cmsmasters_column_inner"><div id="cmsmasters_heading_jvkj8m3sl" class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                    <h3 class="cmsmasters_heading">start your 30-day Digital access trial</h3>
                                </div><div id="cmsmasters_divider_tao1h3iq69" class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div><div id="cmsmasters_heading_hor9gfqbzf" class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                    <h2 class="cmsmasters_heading">Try all of Good Food’s features with our 30-days free trial.</h2>
                                </div><div id="cmsmasters_button_u1f3wruub" class="button_wrap"><a href="/contacts/" class="cmsmasters_button"><span>Free trial</span></a></div>
                            </div></div>
                        <div id="cmsmasters_column_lyh1gf32fd" class="cmsmasters_column one_fourth">
                            <div class="cmsmasters_column_inner"><div class="cmsmasters_pricing_table pricing_one">
                                    <div id="cmsmasters_pricing_item_ipsr217g5e" class="cmsmasters_pricing_item">
                                        <div class="cmsmasters_pricing_item_inner">
                                            <div class="cmsmasters_price_wrap">
                                                <span class="cmsmasters_currency">$</span>
                                                <span class="cmsmasters_price">19.99</span>
                                                <br><span class="cmsmasters_period">Per Year</span>
                                            </div>
                                            <div class="pricing_title_wrap">
                                                <h6 class="pricing_title">Magazine</h6>
                                            </div>
                                            <ul class="feature_list">
                                                <li>6 issues per year</li>
                                                <li>Beautifully printed</li>
                                                <li>No ads, ever</li>
                                                <li>Free home shipping</li>
                                            </ul>
                                            <a href="/contacts/" class="cmsmasters_button"><span>Purchase</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div></div>
                        <div id="cmsmasters_column_1eouvzq8mn" class="cmsmasters_column one_fourth">
                            <div class="cmsmasters_column_inner"><div class="cmsmasters_pricing_table pricing_one">
                                    <div id="cmsmasters_pricing_item_52p6hjsthn" class="cmsmasters_pricing_item">
                                        <div class="cmsmasters_pricing_item_inner">
                                            <div class="cmsmasters_price_wrap">
                                                <span class="cmsmasters_currency">$</span>
                                                <span class="cmsmasters_price">19.99</span>
                                                <br><span class="cmsmasters_period">Per Year</span>
                                            </div>
                                            <div class="pricing_title_wrap">
                                                <h6 class="pricing_title">Digital access</h6>
                                            </div>
                                            <ul class="feature_list">
                                                <li>Every recipe</li>
                                                <li>Every TV &amp; radio show</li>
                                                <li>Every magazine article</li>
                                                <li>Every video</li>
                                            </ul>
                                            <a href="/contacts/" class="cmsmasters_button"><span>Purchase</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div></div>
                        <div id="cmsmasters_column_fihxecf1cq" class="cmsmasters_column one_fourth">
                            <div class="cmsmasters_column_inner"><div class="cmsmasters_pricing_table pricing_one">
                                    <div id="cmsmasters_pricing_item_5wn1tcmrm" class="cmsmasters_pricing_item pricing_best">
                                        <div class="cmsmasters_pricing_item_inner">
                                            <div class="cmsmasters_price_wrap">
                                                <span class="cmsmasters_currency">$</span>
                                                <span class="cmsmasters_price">29.99</span>
                                                <br><span class="cmsmasters_period">Per Year</span>
                                            </div>
                                            <div class="pricing_title_wrap">
                                                <h6 class="pricing_title">Everything</h6>
                                            </div>
                                            <ul class="feature_list">
                                                <li>All things Milk Street</li>
                                                <li>Full digital access</li>
                                                <li>Every recipe</li>
                                                <li>Every magazine issue</li>
                                            </ul>
                                            <a href="/contacts/" class="cmsmasters_button"><span>Purchase</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="cmsmasters_row_28ftb6c6cl" class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
        <div class="cmsmasters_row_outer_parent">
            <div class="cmsmasters_row_outer">
                <div class="cmsmasters_row_inner">
                    <div class="cmsmasters_row_margin cmsmasters_11">
                        <div id="cmsmasters_column_514ip0rnzr" class="cmsmasters_column one_first">
                            <div class="cmsmasters_column_inner"><p></p>
                            </div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="cmsmasters_row_utfeotnfyc" class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
        <div class="cmsmasters_row_outer_parent">
            <div class="cmsmasters_row_outer">
                <div class="cmsmasters_row_inner">
                    <div class="cmsmasters_row_margin cmsmasters_11">
                        <div id="cmsmasters_column_pmz8qms108" class="cmsmasters_column one_first">
                            <div class="cmsmasters_column_inner"><div id="cmsmasters_heading_1vui6lf2g" class="cmsmasters_heading_wrap cmsmasters_heading_align_center">
                                    <h2 class="cmsmasters_heading">CLASSES</h2>
                                </div><div id="cmsmasters_heading_fsx702c6wa" class="cmsmasters_heading_wrap cmsmasters_heading_align_center cmsmasters_heading_shaped">
                                    <h6 class="cmsmasters_heading">Coming soon</h6>
                                </div><div id="cmsmasters_divider_k9y2y2p6qe" class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div><div class="cmsmasters_sidebar sidebar_layout_11"><aside id="tribe-events-adv-list-widget-3" class="widget tribe-events-adv-list-widget">	<p>There are no upcoming events at this time.</p>
                                    </aside><div class="cl"></div>
                                </div><div id="cmsmasters_divider_403pfbwjm" class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div><div id="cmsmasters_button_9oqp6tlpf" class="button_wrap"><a href="/events/list/" class="cmsmasters_button"><span>all classes</span></a></div>
                            </div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="cmsmasters_row_53dt8werg6" class="cmsmasters_row cmsmasters_color_scheme_first cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_fullwidth">
        <div class="cmsmasters_row_outer_parent">
            <div class="cmsmasters_row_outer">
                <div class="cmsmasters_row_inner cmsmasters_row_fullwidth cmsmasters_row_no_margin">
                    <div class="cmsmasters_row_margin cmsmasters_row_columns_behavior cmsmasters_131313">
                        <div id="cmsmasters_column_6717a6c163" class="cmsmasters_column one_third">
                            <div class="cmsmasters_column_inner"><div id="cmsmasters_divider_8phfopf04m" class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div><div id="cmsmasters_heading_0kisdlztbb" class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                    <h3 class="cmsmasters_heading">Buy Gift Cards Online</h3>
                                </div><div id="cmsmasters_divider_oo0493riro" class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div><div id="cmsmasters_heading_52h8v3j6u9" class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                    <h2 class="cmsmasters_heading">Provide with our Gift Card to whom you most want and is sure.</h2>
                                </div><div id="cmsmasters_button_5cdx8c501" class="button_wrap"><a href="/about-us/" class="cmsmasters_button"><span>GIfT Card</span></a></div>
                            </div></div>
                        <div id="cmsmasters_column_47b1d0de2b" class="cmsmasters_column one_third">
                            <div class="cmsmasters_column_inner"><div id="cmsmasters_divider_dn7cz6utz6" class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div><div id="cmsmasters_heading_puox419i3s" class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                    <h3 class="cmsmasters_heading">Subscribe to Goodfood Magazine</h3>
                                </div><div id="cmsmasters_divider_rx3olw7hf" class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div><div id="cmsmasters_heading_zlrqh2exy" class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                    <h2 class="cmsmasters_heading">Get recipes, tips, and news delivered to your inbox.</h2>
                                </div><div class="cmsmasters_mailpoet cmsmasters_custom_mailpoet"><div class="cmsmasters_mailpoet_form">


                                        <div id="mailpoet_form_1" class="mailpoet_form mailpoet_form_shortcode">
                                            <style type="text/css">.mailpoet_hp_email_label{display:none;}#mailpoet_form_1 .mailpoet_form {  }
                                                #mailpoet_form_1 .mailpoet_paragraph { line-height: 20px; }
                                                #mailpoet_form_1 .mailpoet_segment_label, #mailpoet_form_1 .mailpoet_text_label, #mailpoet_form_1 .mailpoet_textarea_label, #mailpoet_form_1 .mailpoet_select_label, #mailpoet_form_1 .mailpoet_radio_label, #mailpoet_form_1 .mailpoet_checkbox_label, #mailpoet_form_1 .mailpoet_list_label, #mailpoet_form_1 .mailpoet_date_label { display: block; font-weight: bold; }
                                                #mailpoet_form_1 .mailpoet_text, #mailpoet_form_1 .mailpoet_textarea, #mailpoet_form_1 .mailpoet_select, #mailpoet_form_1 .mailpoet_date_month, #mailpoet_form_1 .mailpoet_date_day, #mailpoet_form_1 .mailpoet_date_year, #mailpoet_form_1 .mailpoet_date { display: block; }
                                                #mailpoet_form_1 .mailpoet_text, #mailpoet_form_1 .mailpoet_textarea { width: 200px; }
                                                #mailpoet_form_1 .mailpoet_checkbox {  }
                                                #mailpoet_form_1 .mailpoet_submit input {  }
                                                #mailpoet_form_1 .mailpoet_divider {  }
                                                #mailpoet_form_1 .mailpoet_message {  }
                                                #mailpoet_form_1 .mailpoet_validate_success { color: #468847; }
                                                #mailpoet_form_1 .mailpoet_validate_error { color: #b94a48; }</style>
                                            <form target="_self" method="post" action="/wp-admin/admin-post.php?action=mailpoet_subscription_form" class="mailpoet_form mailpoet_form_shortcode" novalidate="">
                                                <input type="hidden" name="data[form_id]" value="1">
                                                <input type="hidden" name="token" value="0ed15785cf">
                                                <input type="hidden" name="api_version" value="v1">
                                                <input type="hidden" name="endpoint" value="subscribers">
                                                <input type="hidden" name="mailpoet_method" value="subscribe">

                                                <label class="mailpoet_hp_email_label">Please leave this field empty<input type="email" name="data[email]"></label><p class="mailpoet_paragraph"><input type="email" class="mailpoet_text" name="data[form_field_ZW1haWw=]" title="Enter Your Email..." value="" data-automation-id="form_email" placeholder="Enter Your Email... *" data-parsley-required="true" data-parsley-minlength="6" data-parsley-maxlength="150" data-parsley-error-message="Please specify a valid email address." data-parsley-required-message="This field is required."></p>
                                                <p class="mailpoet_paragraph"><input type="submit" class="mailpoet_submit" value="Subscribe" data-automation-id="subscribe-submit-button"><span class="mailpoet_form_loading"><span class="mailpoet_bounce1"></span><span class="mailpoet_bounce2"></span><span class="mailpoet_bounce3"></span></span></p>

                                                <div class="mailpoet_message">
                                                    <p class="mailpoet_validate_success" style="display:none;">Check your inbox or spam folder to confirm your subscription.
                                                    </p>
                                                    <p class="mailpoet_validate_error" style="display:none;">        </p>
                                                </div>
                                            </form>
                                        </div>

                                    </div></div>
                            </div></div>
                        <div id="cmsmasters_column_b7f9dd2e6e" class="cmsmasters_column one_third">
                            <div class="cmsmasters_column_inner"><div id="cmsmasters_divider_icbhi2sgcc" class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div><div id="cmsmasters_heading_xfdgrk06x" class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                    <h3 class="cmsmasters_heading">Visit our Store</h3>
                                </div><div id="cmsmasters_divider_qve45x4bkd" class="cmsmasters_divider cmsmasters_divider_width_long cmsmasters_divider_pos_center"></div><div id="cmsmasters_heading_ak6xcco3x8" class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                    <h2 class="cmsmasters_heading">Here you’ll find a carefully chosen kitchen inventory.</h2>
                                </div><div id="cmsmasters_button_wfvdd53hdl" class="button_wrap"><a href="/shop/" class="cmsmasters_button"><span>Explore Shop</span></a></div>
                            </div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="cl"></div><div class="content_wrap fullwidth">

        <div class="middle_content entry"></div>
        <!-- Finish Content -->



    </div>
</div>