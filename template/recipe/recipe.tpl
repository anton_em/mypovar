<!-- Start Middle -->
<div id="middle">
    <div class="headline cmsmasters_color_scheme_default">
        <div class="headline_outer cmsmasters_headline_disabled">
            <div class="headline_color"></div>
        </div>
    </div>
    <div class="middle_inner">
        <div class="content_wrap fullwidth">
            <!-- Start Content -->
            <div class="middle_content entry">
                <div class="portfolio opened-article">
                    <!-- Start Project Single Article -->
                    <article id="post-13125" class="cmsmasters_open_project post-13125 project type-project status-publish format-standard has-post-thumbnail hentry pj-categs-baking" itemscope="" itemtype="http://schema.org/Recipe">
                        <div class="cmsmasters_open_project_top_wrap entry-meta">
                            <header class="cmsmasters_project_header entry-header">
                                <h1 class="cmsmasters_project_title entry-title">[title]</h1>
                            </header>
                            <div class="cmsmasters_open_project_top_meta with_subtitle"><span class="cmsmasters_project_date"><abbr class="published" title="Jun 28, 2017">Jun 28, 2017</abbr><abbr class="dn date updated" title="Jul 19, 2017">Jul 19, 2017</abbr></span>
                               <!-- <span class="cmsmasters_likes cmsmasters_project_likes"><a href="#" onclick="cmsmastersLike(13125, false); return false;" id="cmsmastersLike-13125" class="cmsmastersLike cmsmasters_theme_icon_like"><span>13 </span></a></span><span class="cmsmasters_views cmsmasters_project_views"><span class="cmsmastersView cmsmasters_theme_icon_view no_active"><span>323</span></span></span>-->
                            </div>
                            <div class="cmsmasters_project_subtitle">
                                <h6 class="entry-subtitle">ГОТОВЬТЕ С НАМИ: ЛЕГКИЕ, СВЕЖИЕ И КРАСОЧНЫЕ БЛЮДА</h6>
                            </div>
                        </div>

                        <div class="cmsmasters_open_project_left_wrap">
                            <figure class="cmsmasters_img_wrap" itemscope itemprop="image" itemtype="http://schema.org/ImageObject"><a itemprop="url" href="[main_img_url]" title="[title]" rel="ilightbox[img_13125_5ca382638f706]" class="cmsmasters_img_link preloader highImg"><img width="810" height="630" src="[main_img_url]" class="full-width wp-post-image" alt="Cherry Pie" title="Cherry Pie" sizes="(max-width: 810px) 100vw, 810px" /></a></figure>
                            <div class="cmsmasters_project_cont_info entry-meta"><span class="cmsmasters_project_author">by <a href="#author" title="Posts by cmsmasters" class="vcard author" rel="author"><span class="fn">cmsmasters</span></a></span><span class="cmsmasters_project_category">in <a href="#baking" class="cmsmasters_cat_color cmsmasters_cat_38" rel="category tag">Baking</a></span></div>
                            <!-- <div class="cmsmasters_project_info entry-meta">
                                 <div class="cmsmasters_project_info_item">
                                     <div class="cmsmasters_project_info_item_inner">
                                         <h4 class="entry-title">Prep</h4><span>30 min</span>
                                     </div>
                                 </div>
                                 <div class="cmsmasters_project_info_item">
                                     <div class="cmsmasters_project_info_item_inner">
                                         <h4 class="entry-title">Cook</h4><span>45 min</span>
                                     </div>
                                 </div>
                                 <div class="cmsmasters_project_info_item">
                                     <div class="cmsmasters_project_info_item_inner">
                                         <h4 class="entry-title">Ready In</h4><span>3 h</span>
                                     </div>
                                 </div>
                                 <div class="cmsmasters_project_info_item">
                                     <div class="cmsmasters_project_info_item_inner">
                                         <h4 class="entry-title">Level </h4><span>Difficult</span>
                                     </div>
                                 </div>
                                 <div class="cmsmasters_project_info_item cmsmasters_rating">
                                     <div class="cmsmasters_project_info_item_inner">
                                         <h4 class="entry-title">Rating</h4>
                                         <div class="cmsmasters_rating_wrap">
                                             <div id="post-ratings-13125" class="post-ratings" data-nonce="50f560195d"><img id="rating_13125_1" src="/wp-content/themes/good-food/cmsmasters-wp-postratings/cmsmasters-framework/theme-style/images/stars(png)/rating_on.png" alt="1 Star" title="1 Star" style="cursor: pointer;border: 0px" /><img id="rating_13125_2" src="/wp-content/themes/good-food/cmsmasters-wp-postratings/cmsmasters-framework/theme-style/images/stars(png)/rating_on.png" alt="2 Stars" title="2 Stars" style="cursor: pointer;border: 0px" /><img id="rating_13125_3" src="/wp-content/themes/good-food/cmsmasters-wp-postratings/cmsmasters-framework/theme-style/images/stars(png)/rating_on.png" alt="3 Stars" title="3 Stars" style="cursor: pointer;border: 0px" /><img id="rating_13125_4" src="/wp-content/themes/good-food/cmsmasters-wp-postratings/cmsmasters-framework/theme-style/images/stars(png)/rating_on.png" alt="4 Stars" title="4 Stars" style="cursor: pointer;border: 0px" /><img id="rating_13125_5" src="/wp-content/themes/good-food/cmsmasters-wp-postratings/cmsmasters-framework/theme-style/images/stars(png)/rating_off.png" alt="5 Stars" title="5 Stars" style="cursor: pointer;border: 0px" />
                                                 <div></div>
                                                 <div>
                                                     <div></div>
                                                 </div>
                                                 <div></div>
                                             </div>
                                             <div id="post-ratings-13125-loading" class="post-ratings-loading">
                                                 <img src="/wp-content/plugins/wp-postratings/images/loading.gif" width="16" height="16" class="post-ratings-image" />Loading...</div>
                                         </div>
                                     </div>
                                 </div> -->
                             </div>

                            <table class="cmsmasters_project_ingredients">
                                <h3 class="cmsmasters_project_ingredients_title">Ингредиенты</h3>
                                [ingredients]
                            </table>
                            <!--
                            <div class="project_features entry-meta">
                                <h3 class="project_features_title">Nutrision Information</h3>
                                <div class="project_features_item">
                                    <div class="project_features_item_title">Servings Per Recipe </div>
                                    <div class="project_features_item_desc">12</div>
                                </div>
                                <div class="project_features_item">
                                    <div class="project_features_item_title">Calories</div>
                                    <div class="project_features_item_desc">554</div>
                                </div>
                                <div class="project_features_item">
                                    <div class="project_features_item_title">Sugar</div>
                                    <div class="project_features_item_desc">52g</div>
                                </div>
                                <div class="project_features_item">
                                    <div class="project_features_item_title">Fat</div>
                                    <div class="project_features_item_desc">25g</div>
                                </div>
                            </div>
                            -->
                        </div>
                        <div class="cmsmasters_open_project_right_wrap">
                            <h5 class="cmsmasters_project_excerpt" itemprop="description">
                                [shot_description]
                            </h5>
                            <table class="cmsmasters_project_instructions" itemprop="description">
                                <h3 class="cmsmasters_project_instructions_title">Способ приготовления</h3>
                                [steps]
                            </table>
                            <div class="cmsmasters_project_content entry-content">
                                <div id="cmsmasters_row_befcafe7df" class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
                                    <div class="cmsmasters_row_outer_parent">
                                        <div class="cmsmasters_row_outer">
                                            <div class="cmsmasters_row_inner">
                                                <div class="cmsmasters_row_margin">
                                                    <div id="cmsmasters_column_d8a1cf8c90" class="cmsmasters_column one_first">
                                                        <div class="cmsmasters_column_inner">
                                                            <div class="cmsmasters_text">
                                                                <p>
                                                                    [full_description]
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cmsmasters_print_button_wrap"><a href="#" class="cmsmasters_print_button cmsmasters-icon-print-3" onclick="window.print();return false;">Распечатать</a></div>
                            <!--<aside class="share_posts">
                                <div class="share_posts_inner">
                                    <h2 class="share_posts_title">Share:</h2>
                                    <a href="#">Facebook</a>
                                    <a href="#">Google+</a>
                                    <a href="#">Twitter</a>
                                    <a href="#">Pinterest</a>
                                </div>
                            </aside> -->
                            <aside class="post_nav"><span class="cmsmasters_prev_post">
                                    <a href="[prev_url]" rel="prev">
                                        [prev_title]
                                    </a>
                                    <span class="cmsmasters_prev_arrow"><span></span></span></span>
                                <span class="cmsmasters_next_post">
                                    <a href="[next_url]" rel="next">[next_title]</a>
                                    <span class="cmsmasters_next_arrow"><span></span></span></span>
                            </aside>

                            <aside class="cmsmasters_single_slider">
                                <h3 class="cmsmasters_single_slider_title">Похожие рецепты</h3>
                                <div class="cmsmasters_single_slider_inner">
                                    <div id="cmsmasters_owl_slider_5ca382639193b" class="cmsmasters_owl_slider" data-single-item="false" data-pagination="false" data-auto-play="5000">
                                        [similar_recipes_view]
                                    </div>
                                </div>
                            </aside>
                            <!--
                            <div id="respond" class="comment-respond">
                                <h3 id="reply-title" class="comment-reply-title">Leave a Reply <small><a rel="nofollow" id="cancel-comment-reply-link" href="/recipe/cherry-pie/#respond" style="display:none;">Cancel Reply</a></small></h3>
                                <form action="/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate>
                                    <p class="comment-notes">Your email address will not be published.</p>
                                    <p class="comment-form-author">
                                        <label for="author">Name<span class="cmsmasters_req">*</span></label>
                                        <input type="text" id="author" name="author" value="" size="35" />
                                    </p>
                                    <p class="comment-form-email">
                                        <label for="email">Email<span class="cmsmasters_req">*</span></label>
                                        <input type="text" id="email" name="email" value="" size="35" />
                                    </p>
                                    <p class="comment-form-cookies-consent">
                                        <input type="checkbox" id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" value="yes" />
                                        <label for="wp-comment-cookies-consent">Save my name, email, and website in this browser for the next time I comment.</label>
                                    </p>
                                    <p class="comment-form-comment"><label for="comment">Comment</label><textarea name="comment" id="comment" cols="67" rows="2"></textarea></p>
                                    <p class="form-submit"><input name="submit" type="submit" id="submit" class="submit" value="Add Comment" /> <input type='hidden' name='comment_post_ID' value='13125' id='comment_post_ID' />
                                        <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
                                    </p>
                                    <p style="display: none;"><input type="hidden" id="akismet_comment_nonce" name="akismet_comment_nonce" value="b7eec7a5cd" /></p>
                                    <p style="display: none;"><input type="hidden" id="ak_js" name="ak_js" value="152" /></p>
                                </form>
                            </div>
                            -->
                        </div>
                    </article>
                    <!-- Finish Project Single Article -->
                </div>
            </div>
            <!-- Finish Content -->
        </div>
    </div>
</div>
<!-- Finish Middle -->