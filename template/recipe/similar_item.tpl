<div class="cmsmasters_owl_slider_item cmsmasters_single_slider_item">
    <div class="cmsmasters_single_slider_item_outer">
        <figure class="cmsmasters_img_wrap" itemscope itemprop="image" itemtype="http://schema.org/ImageObject">
            <a itemprop="url" href="[similar_url]" title="Thai Buffalo Wings" class="cmsmasters_img_link preloader">
                <img width="810" height="630" src="[similar_img]" class="full-width wp-post-image" alt="[title]" title="[title]">
            </a>
        </figure>
        <div class="cmsmasters_single_slider_item_inner">
            <h4 class="cmsmasters_single_slider_item_title">
                <a href="[similar_url]">[title]</a>
            </h4>
        </div>
    </div>
</div>